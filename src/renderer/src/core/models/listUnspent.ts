export interface ITokenListUnspent {
    cursor?: any;
    unspent: ITokenUtxo[];
}

export interface ITokenUtxo {
    group: string;
    height: number;
    outpoint_hash: string;
    token_amount: number | bigint;
    token_id_hex: string;
    tx_hash: string;
    tx_pos: number;
    value: number;
}

export interface IListUnspentRecord {
    has_token: boolean;
    height: number;
    outpoint_hash: string;
    tx_hash: string;
    tx_pos: number;
    value: number;
}

export interface IUtxo {
    addresses: string[];
    amount: number;
    group: string;
    group_authority: bigint | number;
    group_quantity: bigint | number;
    height: number;
    scripthash: string;
    scriptpubkey: string;
    spent: ISpent;
    status: string;
    template_argumenthash: string;
    template_scripthash: string;
    token_id_hex: string;
    tx_hash: string;
    tx_idem: string;
    tx_pos: number;
}

export interface ISpent {
    height: number;
    tx_hash: string;
    tx_pos: number;
}