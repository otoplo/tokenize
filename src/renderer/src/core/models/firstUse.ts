export interface IFirstUse {
    block_hash: string;
    block_height: number;
    height: number;
    tx_hash: string;
}