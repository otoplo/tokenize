export type RostrumTransportScheme = 'ws' | 'wss';

export const RostrumScheme = {
    WS: 'ws',
    WSS: 'wss'
}

export interface RostrumParams {
    scheme: RostrumTransportScheme;
    host: string;
    port: number;
}