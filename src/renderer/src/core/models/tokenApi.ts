export interface IErrorResponse {
    code: string;
    message: string;
}

export interface ITokenResponse {
    tokenInfo: ITokenMetadata;
    iconUrl: string
    jsonDoc: string;
}

export interface ITokenMetadata {
    id: number;
    token: string;
    tokenIdHex: string;
    name: string;
    ticker: string;
    documentUrl: string;
    documentHash: string;
    decimals: number;
    parentGroup: string;
    genesisTx: string;
    genesisAddress: string;
}

export interface ISupplyResponse {
    token: string;
    supply: number | bigint | string;
    rawSupply: number | bigint | string;
}