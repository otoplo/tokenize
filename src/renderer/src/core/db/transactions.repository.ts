import { appdb } from "./app-db";
import { TransactionEntity } from "./entities";

class TransactionsRepository {

  public async upsert(tx: TransactionEntity) {
    return await appdb.transactions.put(tx, tx.idem);
  }

  public async findAll() {
    return await appdb.transactions.orderBy('time').reverse().toArray();
  }

  public async count() {
    return await appdb.transactions.count();
  }

  public async findByGroup(group?: string) {
    if (group) {
      return await appdb.transactions.where('group').equals(group).or('extraGroup').equals(group).reverse().sortBy('time');
    }
    return undefined;
  }

  public async findByIdem(idem: string) {
    return await appdb.transactions.where('idem').equals(idem).first();
  }
}

export const transactionsRepository = new TransactionsRepository();