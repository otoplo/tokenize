import axios, { AxiosResponse } from "axios";
import { IErrorResponse, ISupplyResponse, ITokenResponse } from "../core/models/tokenApi";

export default class TokenApiProvider {

    private static readonly endpoint = "https://tokenapi.otoplo.com/api/v1/";

    public static async getTokenInfo(token: string) {
        return await this.performGet<ITokenResponse>("tokens/" + token + "/metadata");
    }
    
    public static async getTokenSupply(token: string) {
        return await this.performGet<ISupplyResponse>("tokens/" + token + "/supply");
    }

    public static async isTokenExist(token: string) {
        try {
            await TokenApiProvider.getTokenInfo(token);
            return true;
        } catch {
            return false
        }
    }

    public static async getJsonDoc(url: string) {
        let res: AxiosResponse<string, any>;
        try {
            res = await axios.get<string>(url, { transformResponse: (res) => res });
        } catch {
            throw new Error(`Failed to get json from: ${url}`);
        }
    
        try {
            let json = JSON.parse(res.data);
            return { json: json, jsonStr: res.data };
        } catch (e) {
            throw new Error(`Failed to parse json ${e instanceof Error ? ': ' + e.message : ''}`);
        }
    }

    private static async performGet<T>(url: string) {
        try {
            const { data } = await axios.get<T>(this.endpoint + url);
            return data;
        } catch (e) {
            if (axios.isAxiosError<IErrorResponse>(e) && e.response?.data) {
                throw new Error(e.response.data.message);
            } else {
                console.error(e);
                throw new Error("Unexpected Error: " + (e instanceof Error ? e.message : "see log for details"));
            }
        }
    }
}