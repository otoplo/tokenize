import { RostrumParams, RostrumScheme } from "../core/models/rostrumParams";
import { TxStatus, WalletIndexes } from "../core/wallet/interfaces";

export default class StorageProvider {
    public static readonly SYNC_LOCK = "syncing";

    public static getEncryptedMnemonic() {
        return localStorage.getItem('mnemonic');
    }
    
    public static saveEncryptedMnemonic(mnemonic: string) {
        localStorage.setItem('mnemonic', mnemonic);
    }
    
    public static getWalletIndexes(): WalletIndexes {
        var idx = localStorage.getItem("wallet-idx");
        return idx !== null ? JSON.parse(idx) : {receiveIndex: 0, changeIndex: 0};
    }
    
    public static saveWalletIndexes(indexes: WalletIndexes) {
        localStorage.setItem('wallet-idx', JSON.stringify(indexes));
    }
    
    public static setTransactionsStatus(status: TxStatus) {
        localStorage.setItem("tx-status", JSON.stringify(status));
    }
    
    public static getTransactionsStatus(): TxStatus {
        var status = localStorage.getItem("tx-status");
        return status !== null ? JSON.parse(status) : {height: 0};
    }
    
    public static getRostrumParams(): RostrumParams {
        var params = localStorage.getItem("rostrum-params");
        return params !== null ? JSON.parse(params) : {scheme: RostrumScheme.WSS, host: 'rostrum.otoplo.com', port: 443};
    }

    public static saveRostrumParams(params: RostrumParams) {
        localStorage.setItem("rostrum-params", JSON.stringify(params));
    }

    public static removeRostrumParams() {
        localStorage.removeItem("rostrum-params")
    }
    
    public static clearData() {
        localStorage.clear();
    }
    
    public static setLock(lock: string) {
        if (localStorage.getItem(lock) == null) {
            localStorage.setItem(lock, "true");
            return true;
        }
        return false;
    }
    
    public static removeLock(lock: string) {
        localStorage.removeItem(lock);
    }
    
    public static setLastSync() {
        var time = Math.floor(Date.now() / 1000);
        localStorage.setItem("last-sync", time.toString());
    }
    
    public static getLastSync() {
        var time = localStorage.getItem("last-sync");
        return time ? parseInt(time) : null;
    }
}