import { useEffect, useRef, ComponentType } from 'react';

interface Props {
    onClose: () => void;
}

export function useCloseByClickOutside(
    WrappedComponent: ComponentType<Props>,
): ComponentType<Props> {
    return function CloseByClickOutside(props: Props) {
        const { onClose } = props;
        const wrapperRef = useRef<HTMLDivElement>(null);

        useEffect(() => {
            const handleClickOutside = (event: MouseEvent) => {
                if (
                    wrapperRef.current &&
                    !wrapperRef.current.contains(event.target as Node)
                ) {
                    onClose();
                }
            };

            document.addEventListener('mousedown', handleClickOutside);

            return () => {
                document.removeEventListener('mousedown', handleClickOutside);
            };
        }, [onClose]);

        return (
            <div ref={wrapperRef}>
                <WrappedComponent onClose={onClose} />
            </div>
        );
    };
}
