import bigDecimal from "js-big-decimal";
import nexcore from 'nexcore-lib';
import PrivateKey from "nexcore-lib/types/lib/privatekey";

export const MAX_INT64: bigint = 9223372036854775807n;

export function isNullOrEmpty<T>(arg: string | Array<T>) {
    return !arg || arg.length === 0;
}

export const truncateStringMiddle = (str?: string, maxLength: number = 0) => {
    if (!str || str.length <= maxLength) {
        return str;
    }

    const ellipsis: string = '...';
    const halfLength: number = Math.floor((maxLength - ellipsis.length) / 2);
    const firstHalf: string = str.slice(0, halfLength);
    const secondHalf: string = str.slice(str.length - halfLength);

    return firstHalf + ellipsis + secondHalf;
};

export function currentTimestamp() {
    return Math.floor(Date.now() / 1000);
}

export function parseAmountWithDecimals(amount: string | number | bigint, decimals: number) {
    let val = new bigDecimal(amount).divide(new bigDecimal(Math.pow(10, decimals)), decimals).getPrettyValue();
    if (val.match(/\./)) {
        val = val.replace(/\.?0+$/, '');
    }
    return val;
}

export function getRawAmount(amount: string | number | bigint, decimals: number) {
    return new bigDecimal(amount).multiply(new bigDecimal(Math.pow(10, decimals))).getValue();
}

export function isValidUrl(s: string, protocols: string[]) {
    try {
        let url = new URL(s);
        return protocols
            ? url.protocol
                ? protocols.map(x => `${x.toLowerCase()}:`).includes(url.protocol)
                : false
            : true;
    } catch (err) {
        return false;
    }
}

export function calcJsonDocHash(jsonDoc: string) {
    let json = jsonDoc.substring(jsonDoc.indexOf('{'), jsonDoc.lastIndexOf('}') + 1);
    return nexcore.crypto.Hash.sha256(Buffer.from(json)).toString('hex');
}

export function signJsonDoc(jsonStr: string, key: PrivateKey) {
    return nexcore.GroupToken.signJsonDoc(jsonStr, key);
}