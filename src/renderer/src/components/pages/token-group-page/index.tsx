import { SetStateAction, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Table from '../../organism/table';
import TokenCard, { Permission, TX, TokenCardDataDynamic, AuthorityData } from '../../atoms/token-card';
import NftsList from '../../molecules/nfts-list';
import TableTitle from '../../atoms/table-title';
import { AuthorisePopup, ReceivePopup, KillAuthorityPopup, RescriptPopup, SendPopup, MeltPopup, MintPopup } from '../../molecules/popups';
import Popup from '../../organism/popup';
import styles from './token-group-page.module.css';
import { useLiveQuery } from 'dexie-react-hooks';
import { tokensRepository } from '@renderer/core/db/tokens.repository';
import { transactionsRepository } from '@renderer/core/db/transactions.repository';
import TokenApiProvider from '@renderer/providers/tokenApiProvider';
import { classifyAuthority, fetchTokenUtxos } from '@renderer/core/wallet/tokenUtils';
import { useAppDispatch, useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import { parseAmountWithDecimals } from '@renderer/utils/utils';
import { startLoading, stopLoading } from '@renderer/store/slices/loading';
import { setTokenData, tokensState } from '@renderer/store/slices/tokens';
import { NftEntity, TokenEntity } from '@renderer/core/db/entities';
import { parseTokenTxAction } from '@renderer/core/wallet/walletUtils';
import { ITokenUtxo } from '@renderer/core/models/listUnspent';
import { ISupplyResponse } from '@renderer/core/models/tokenApi';
import { nftsRepository } from '@renderer/core/db/nfts.repository';
import NiftyProvider from '@renderer/providers/niftyProvider';

const tableAuthorityHeaders = [
    'authorise',
    'mint',
    'melt',
    'rescript',
    'subgroup',
    'kill',
];
const tableTransactionsHeaders = ['type', 'txidem', 'amount', 'date'];

const initKillPopupData = {
    authorise: false,
    melt: false,
    mint: false,
    rescript: false,
    subgroup: false,
    outpoint: '',
};

const initGroupInfo: TokenEntity = {
    token: '',
    tokenIdHex: '',
    addedTime: 0,
    decimals: 0,
    docHash: '',
    docUrl: '',
    iconUrl: '',
    name: '',
    ticker: '',
    parentGroup: '',
    summary: '',
}

export default function TokenGroupPage() {
    const [popUpStatus, setPopUpStatus] = useState<string | null>();
    const [showPopUp, setShowPopUp] = useState<boolean>(false);
    const [killPopupData, setKillPopupData] = useState<AuthorityData>(initKillPopupData);
    const [killPopupAllData, setKillPopupAllData] = useState<AuthorityData[]>([]);
    const [nfts, setNfts] = useState<NftEntity[]>([]);

    const wallet = useAppSelector(walletState);
    const tokens = useAppSelector(tokensState);
    const dispatch = useAppDispatch();

    const navigate = useNavigate();
    const { id } = useParams();

    const cardData = tokens[id ?? ''];

    const groupInfo = useLiveQuery(() => tokensRepository.findById(id), [id]);
    const txList = useLiveQuery(() => transactionsRepository.findByGroup(id), [id]);
    const nftList = useLiveQuery(() => nftsRepository.findAllByParent(id), [id]);

    const isNFT = groupInfo?.tokenIdHex === NiftyProvider.NIFTY_TOKEN.tokenIdHex;

    useEffect(() => {
        //solve visual bug in swiper
        if (nftList) {
            setNfts(nftList);
        }
    }, [nftList])

    useEffect(() => {
        async function loadData() {
            if (!groupInfo || !id) {
                return;
            }

            let supplyRes: ISupplyResponse;
            let supply = '';
            try {
                supplyRes = await TokenApiProvider.getTokenSupply(id);
                supply = parseAmountWithDecimals(supplyRes.rawSupply, groupInfo.decimals);
            } catch (e) {
                // it might not yet added to token api
                supplyRes = { rawSupply: "0", supply: "0", token: id };
                supply = 'unknown'
            }

            let rAddrs = wallet.keys.receiveKeys.map(a => a.address);
            let cAddrs = wallet.keys.changeKeys.map(a => a.address);
            let utxos = txList && txList.length > 0 ? (await fetchTokenUtxos(rAddrs.concat(cAddrs), groupInfo.token)) : [];

            let authorities = new Set<string>();
            let [balance, pending, auths] = buildAuthsAndBalance(utxos, authorities);
            let perms = buildPermissions(authorities);

            let cardData: TokenCardDataDynamic = {
                amount: { label: "Supply", amount: supply, rawAmount: supplyRes.rawSupply },
                balance: balance,
                pending: pending,
                permissions: perms,
                authorities: auths
            };

            dispatch(setTokenData({ id: groupInfo.token, data: cardData }));
        }

        if (!cardData) {
            dispatch(startLoading());
        }

        loadData().catch(e => {
            // TODO
        }).finally(() => {
            dispatch(stopLoading());
        });
    }, [txList])

    const buildTxs = () => {
        let txs: TX[] = [];
        txList?.forEach(tx => {
            let act = parseTokenTxAction(tx.txGroupType, tx.state);
            if (groupInfo?.token === tx.extraGroup) {
                act = 'subgroup ' + act;
            }
            let t: TX = {
                action: act,
                height: tx.height,
                timestamp: tx.time,
                txidem: tx.idem,
                amount: parseAmountWithDecimals(tx.tokenAmount, groupInfo!.decimals)
            };
            txs.push(t);
        });
        return txs;
    }

    const buildAuthsAndBalance = (utxos: ITokenUtxo[][], authorities: Set<string>): [bigint, bigint, AuthorityData[]] => {
        let balance = { confirmed: 0n, uncofrimed: 0n };
        let auths: AuthorityData[] = [];
        utxos.forEach(utxoList => {
            utxoList.forEach(utxo => {
                if (utxo.token_amount > 0n) {
                    if (utxo.height > 0) {
                        balance.confirmed += BigInt(utxo.token_amount);
                    } else {
                        balance.uncofrimed += BigInt(utxo.token_amount);
                    }
                } else {
                    let authsSet = classifyAuthority(BigInt(utxo.token_amount), authorities);
                    auths.push({
                        authorise: authsSet.has('baton'),
                        mint: authsSet.has('mint'),
                        melt: authsSet.has('melt'),
                        rescript: authsSet.has('rescript'),
                        subgroup: authsSet.has('subgroup'),
                        outpoint: utxo.outpoint_hash
                    });
                }
            });
        });
        return [balance.confirmed, balance.uncofrimed, auths];
    }

    const buildPermissions = (authorities: Set<string>): Permission[] => {
        return [
            {
                label: 'authorise',
                state: authorities.has('baton') ? 'active' : 'disabled'
            },
            {
                label: 'mint',
                state: authorities.has('mint') ? 'active' : 'disabled'
            },
            {
                label: 'melt',
                state: authorities.has('melt') ? 'active' : 'disabled'
            },
            {
                label: 'rescript',
                state: authorities.has('rescript') ? 'active' : 'disabled'
            },
            {
                label: 'subgroup',
                state: authorities.has('subgroup') ? 'active' : 'disabled'
            }
        ]
    }

    const handleOpenPopup = (popUp: string) => {
        setShowPopUp(true);
        setPopUpStatus(popUp);
    };

    const handleClosePopUp = () => {
        setShowPopUp(false);
        setPopUpStatus(null);
    };

    const handleOnKill = (data: SetStateAction<AuthorityData>) => {
        setKillPopupData(data);
        setKillPopupAllData(cardData.authorities);
        handleOpenPopup('kill-authority-popup');
    };

    const handleClickToggle = (label: string) => {
        switch (label) {
            case 'authorise':
                handleOpenPopup('authorise-popup');
                break;
            case 'mint':
                handleOpenPopup('mint-popup');
                break;
            case 'melt':
                handleOpenPopup('melt-popup');
                break;
            case 'rescript':
                handleOpenPopup('rescript-popup');
                break;
            case 'subgroup':
                navigate('/create-token-group-page', { state: { parent: groupInfo?.token } });
                break;
            case 'receive':
                handleOpenPopup('receive-popup');
                break;
            case 'send':
                handleOpenPopup('send-popup');
                break;
            default:
                break;
        }
    };

    const renderPopup = () => {
        switch (popUpStatus) {
            case 'send-popup':
                return <SendPopup token={groupInfo?.token || ''} ticker={groupInfo?.ticker || ''} decimals={groupInfo?.decimals || 0} close={handleClosePopUp}/>;
            case 'receive-popup':
                return <ReceivePopup />;
            case 'authorise-popup':
                return <AuthorisePopup token={groupInfo?.token || ''} ticker={groupInfo?.ticker || ''} allPerms={cardData.permissions} close={handleClosePopUp}/>;
            case 'mint-popup':
                return <MintPopup token={groupInfo?.token || ''} ticker={groupInfo?.ticker || ''} currentSupply={cardData.amount.rawAmount.toString()} decimals={groupInfo?.decimals || 0} close={handleClosePopUp}/>
            case 'melt-popup':
                return <MeltPopup token={groupInfo?.token || ''} ticker={groupInfo?.ticker || ''} balance={cardData.balance} currentSupply={cardData!.amount.rawAmount.toString()} decimals={groupInfo?.decimals || 0} close={handleClosePopUp} />
            case 'rescript-popup':
                return <RescriptPopup />;
            case 'kill-authority-popup':
                return <KillAuthorityPopup token={groupInfo?.token || ''} data={killPopupData} allAuths={killPopupAllData} close={handleClosePopUp}/>;
            default:
                return null;
        }
    };

    let txs = buildTxs();

    return (
        <div className={styles['token-group-page']}>
            {showPopUp && (
                <Popup isMini={popUpStatus === 'send-popup'} close={handleClosePopUp}>{renderPopup()}</Popup>
            )}
            <TokenCard
                tokenInfo={groupInfo || initGroupInfo}
                tokenExtendedInfo={cardData}
                isNft={isNFT}
                handleClickToggle={handleClickToggle}
            />
            {isNFT && (
                <NftsList nftsList={nfts} />
            )}
            <div className={styles['table-section']}>
                <TableTitle label={'Authority UTXOS'} />
                <div className={styles['table-container']}>
                    <Table
                        headers={tableAuthorityHeaders}
                        data={cardData?.authorities}
                        tableType={'authority'}
                        onKill={handleOnKill}
                    />
                </div>
            </div>
            <div className={styles['table-section']}>
                <TableTitle label={'Transactions'} />
                <div className={styles['table-container']}>
                    <Table
                        headers={tableTransactionsHeaders}
                        data={txs}
                        tableType={'transactions'}
                    />
                </div>
            </div>
        </div>
    );
}