import AmountLabel from '../../atoms/amount-label';
import PageTitle from '../../atoms/page-title';
import ReceiveSection from '../../molecules/receive-section';
import styles from './manage-wallet-page.module.css';
import bigDecimal from 'js-big-decimal';
import { useAppSelector } from '../../../store/hooks';
import { walletState } from '../../../store/slices/wallet';
import SendSection from '../../molecules/send-section';

export default function ManageWalletPage() {
    const wallet = useAppSelector(walletState);
    const isLoading = wallet.initLoading;

    return (
        <>
            <div className={styles['manage-wallet-page']}>
                <div className={styles['manage-wallet-page-header']}>
                    <PageTitle label="Wallet" />
                    <AmountLabel
                        amount={new bigDecimal(wallet.balance.confirmed)}
                        pending={new bigDecimal(wallet.balance.unconfirmed)}
                        coin={'NEXA'}
                        decimals={2}
                        isLoading={isLoading}
                    />
                </div>
                <div className={styles['manage-wallet-page-content']}>
                    <ReceiveSection />
                    <SendSection ticker='NEXA' decimals={2}/>
                </div>
            </div>
        </>
    );
}
