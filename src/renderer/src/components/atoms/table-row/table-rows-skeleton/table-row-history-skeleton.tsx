import Skeleton from 'react-loading-skeleton';
import styles from '../table-row.module.css';

export default function TableRowHistorySkeleton() {
    return (
        <tr className={styles['tr']}>
            <td className={styles['td-skeleton-date']}>
                <Skeleton height={30} width={113} />
            </td>
            <td className={styles['td-skeleton-assets']}>
                <Skeleton circle width={30} height={30} />
            </td>
            <td className={styles['td-skeleton-txid']}>
                <Skeleton height={30} width={230} />
            </td>
            <td className={styles['td-skeleton-nexa']}>
                <Skeleton height={30} width={153} />
            </td>
            <td className={styles['td-skeleton-tokens']}>
                <Skeleton height={30} width={153} />
            </td>
            <td className={styles['td-skeleton-action']}>
                <Skeleton height={30} width={75} />
            </td>
        </tr>
    );
}
