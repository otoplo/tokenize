import KillSwitch from '../../kill-switch';
import CheckMark from '../../check-mark';
import styles from '../table-row.module.css';

export default function TableRowAuthoritySkeleton() {
    const columns = [
        {
            className: styles['td-autorise-skeleton'],
            component: <CheckMark isLoading={true} />,
        },
        {
            className: styles['td-mint-skeleton'],
            component: <CheckMark isLoading={true} />,
        },
        {
            className: styles['td-melt-skeleton'],
            component: <CheckMark isLoading={true} />,
        },
        {
            className: styles['td-rescript-skeleton'],
            component: <CheckMark isLoading={true} />,
        },
        {
            className: styles['td-subgroup-skeleton'],
            component: <CheckMark isLoading={true} />,
        },
        {
            className: styles['td-kill-skeleton'],
            component: <KillSwitch isLoading={true} />,
        },
    ];

    return (
        <tr className={styles['tr']}>
            {columns.map((column, index) => (
                <td key={index} className={column.className}>
                    {column.component}
                </td>
            ))}
        </tr>
    );
}
