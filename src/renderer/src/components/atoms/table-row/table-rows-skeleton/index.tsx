import React from 'react';
import TableRowHistorySkeleton from './table-row-history-skeleton';
import TableRowAuthoritySkeleton from './table-row-authority-skeleton';
import TableRowKillAuthoritySkeleton from './table-row-kill-authority-skeleton';
import TableRowTransactionsSkeleton from './table-row-transactions-skeleton';

interface TableRowsSkeletonProps {
    rows: number;
    tableType: 'history' | 'authority' | 'transactions' | 'kill_authority';
}

const TableRowsSkeleton: React.FC<TableRowsSkeletonProps> = ({
    rows,
    tableType,
}) => {
    return (
        <>
            {Array(rows)
                .fill('')
                .map((_, index) => {
                    switch (tableType) {
                        case 'history':
                            return <TableRowHistorySkeleton key={index} />;
                        case 'authority':
                            return <TableRowAuthoritySkeleton key={index} />;
                        case 'transactions':
                            return <TableRowTransactionsSkeleton key={index} />;
                        case 'kill_authority':
                            return (
                                <TableRowKillAuthoritySkeleton key={index} />
                            );
                        default:
                            return null;
                    }
                })}
        </>
    );
};

export default TableRowsSkeleton;
