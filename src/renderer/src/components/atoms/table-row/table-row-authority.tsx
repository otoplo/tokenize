import { SetStateAction } from 'react';
import CheckMark from '../check-mark';
import KillSwitch from '../kill-switch';
import styles from './table-row.module.css';
import { AuthorityData } from '../token-card';

export default function TableRowAuthority({
    rowData,
    onKill,
}: {
    rowData: AuthorityData;
    onKill?: (data: SetStateAction<AuthorityData>) => void;
}) {

    return (
        <tr className={styles['tr']}>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.authorise} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.mint} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.melt} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.rescript} />
            </td>
            <td className={styles['td-authority']}>
                <CheckMark isActive={rowData.subgroup} />
            </td>
            {onKill && (
                <td className={styles['td-authority']}>
                    <KillSwitch onClick={() => onKill(rowData)} />
                </td>
            )}
        </tr>
    );
}
