import useScreenSize from '../../../hooks/useScreenSize';
import { truncateStringMiddle } from '../../../utils/utils';
import MiniButton from '../mini-button';
import { TX } from '../token-card';
import styles from './table-row.module.css';

export default function TableRowTransactions({ rowData }: { rowData: TX }) {
    const isSmallScreen = useScreenSize().width < 1919;

    let dateAndTime = "Pending";
    if (rowData.height > 0) {
        let date = new Date(rowData.timestamp * 1000).toLocaleDateString();
        let time = new Date(rowData.timestamp * 1000).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit", hour12: false });
        dateAndTime = date + " " + time;
    }

    let action = rowData.action.includes('subgroup') ? 'subgroup' : rowData.action;

    return (
        <tr className={styles['tr']}>
            <td style={{ color: `var(--${action})` }}>
                {rowData.action}
            </td>
            <td
                style={{
                    color: `var(--hyperlink)`,
                    display: 'flex',
                    alignItems: 'center',
                    height: '50px',
                    gap: '10px',
                }}
            >
                <a
                    href={`https://explorer.nexa.org/tx/${rowData.txidem}`}
                    className={styles['txid']}
                    target={'_blank'}
                    rel="noreferrer"
                >
                    {isSmallScreen
                        ? truncateStringMiddle(rowData.txidem, 28)
                        : rowData.txidem}
                </a>
                <MiniButton text={rowData.txidem} type={'copy'} />
            </td>
            <td>{rowData.amount}</td>
            <td>{dateAndTime}</td>
        </tr>
    );
}
