import cn from 'classnames';
import styles from './toggle-button.module.css';

interface IToggleButton {
    isActive: boolean;
    onClick: () => void;
}

export default function ToggleButton({ isActive, onClick }: IToggleButton) {
    const toggleClass = cn({
        [styles['toggle-button']]: true,
        [styles['toggle-active']]: isActive,
        [styles['toggle-deactivated']]: !isActive,
    });

    return <div onClick={onClick} className={toggleClass} />;
}
