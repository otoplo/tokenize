import { ReactComponent as WarningMarkIcon } from '../../../assets/warning-mark.svg';
import styles from './no-results.module.css';

export default function NoResults() {
    return (
        <div className={styles['main-box']}>
            <WarningMarkIcon />
            <h5>No results found.</h5>
        </div>
    );
}
