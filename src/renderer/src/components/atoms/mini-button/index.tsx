import { useState, useEffect, ClipboardEvent } from 'react';
import cn from 'classnames';
import { ReactComponent as RequestIcon } from '../../../assets/request-icon.svg';
import { ReactComponent as CopyIcon } from '../../../assets/copy-icon.svg';
import { ReactComponent as QrIcon } from '../../../assets/qr.svg';
import { ReactComponent as CompleteIcon } from '../../../assets/checked-tick.svg';
import QrCodeTooltip from '../../molecules/qr-code-tooltip';
import styles from './mini-button.module.css';
import { toast } from 'react-toastify';

function PasteButton({
    onClick,
}: {
    onClick: (event: ClipboardEvent<HTMLInputElement>) => Promise<void>;
}) {
    const handlePaste = (
        event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    ) => {
        onClick(event as unknown as ClipboardEvent<HTMLInputElement>);
    };

    return (
        <div className={styles['img-wrapper']} onClick={handlePaste}>
            <RequestIcon />
        </div>
    );
}

function QrCodeHintButton({ onClick }: { onClick: () => void }) {
    return (
        <div
            className={styles['img-wrapper']}
            onClick={onClick}
            style={{ transform: 'rotate(90deg)' }}
        >
            <QrIcon />
        </div>
    );
}

function CopyButton({ text }: { text: string }) {
    const [clicked, setClicked] = useState<boolean>(false);

    useEffect(() => {
        let timer: NodeJS.Timeout;

        if (clicked) {
            timer = setTimeout(() => {
                setClicked(false);
            }, 2000);
        }

        return () => clearTimeout(timer);
    }, [clicked]);

    const handleCopy = async () => {
        try {
            await navigator.clipboard.writeText(text);
            setClicked(true);
            toast.success("Copied!", {
                position: "top-center",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "dark",
            });
        } catch (error) {
            console.error('Failed to copy text:', error);
        }
    };

    const CopyButtonClass = cn({
        [styles['img-wrapper']]: true,
        [styles['img-wrapper-complete']]: clicked,
    });

    return (
        <div className={CopyButtonClass} onClick={handleCopy}>
            {clicked ? <CompleteIcon /> : <CopyIcon />}
        </div>
    );
}

export default function MiniButton({
    text,
    onClick,
    type,
    url,
    isTokenCard,
}: {
    text?: string;
    onClick?: (event: ClipboardEvent<HTMLInputElement>) => Promise<void>;
    url?: string;
    type: 'paste' | 'copy' | 'qr-hint';
    isTokenCard?: boolean;
}) {
    const [openHint, setOpenHint] = useState(false);

    const handleOpenQr = () => {
        setOpenHint(!openHint);
    };

    const handleCloseQr = () => {
        setOpenHint(false);
    };

    return (
        <>
            <div className={styles['mini-button']}>
                {(() => {
                    switch (type) {
                        case 'qr-hint':
                            return <QrCodeHintButton onClick={handleOpenQr} />;
                        case 'copy':
                            return <CopyButton text={text ?? ''} />;
                        case 'paste':
                            return (
                                <PasteButton
                                    onClick={
                                        onClick as (
                                            event: ClipboardEvent<HTMLInputElement>,
                                        ) => Promise<void>
                                    }
                                />
                            );
                    }
                })()}
            </div>
            {openHint && (
                <QrCodeTooltip
                    url={url ?? ''}
                    isTokenCard={isTokenCard}
                    onClose={handleCloseQr}
                />
            )}
        </>
    );
}
