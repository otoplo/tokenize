import styles from './logo.module.css';
import logo from '../../../assets/tokenize-logo.svg';

export default function Logo() {
    const reload = () => {
        window.location.reload();
    }

    return (
        <span className={styles['logo']} onClick={reload}>
            <img width={200} src={logo} alt="TOKENIZE" />
        </span>
    );
}
