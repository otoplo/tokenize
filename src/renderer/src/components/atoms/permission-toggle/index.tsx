import { MouseEventHandler } from 'react';
import cn from 'classnames';
import styles from './permission-toggle.module.css';

export type PermissionLable = 'authorise' | 'mint' | 'melt' | 'rescript' | 'subgroup';
export type PermissionState = 'active' | 'note' | 'disabled';

interface InterfacePermissionLabel {
    label: PermissionLable | 'Send' | 'Receive';
    state: PermissionState;
    isClickableWhenDisabled?: boolean;
    cursorPointer?: boolean;
    onClick?: MouseEventHandler<HTMLButtonElement>;
}

export default function PermissionToggle({
    label,
    state = 'active',
    isClickableWhenDisabled = false,
    cursorPointer,
    onClick,
}: InterfacePermissionLabel) {
    const permissionToggleClass = cn({
        [styles['permission-toggle']]: true,
        [styles[`${label}-active`]]: state === 'active',
        [styles['active']]: state === 'active',
        [styles[`${label}-note`]]: state === 'note',
        [styles['note']]: state === 'note',
        [styles['disabled']]: state === 'disabled',
        [styles['cursor-pointer']]: cursorPointer,
    });

    return (
        <button
            className={permissionToggleClass}
            onClick={
                isClickableWhenDisabled
                    ? onClick
                    : state === 'active'
                    ? onClick
                    : undefined
            }
        >
            {label}
        </button>
    );
}
