import styles from './tokenize-title.module.css';
import logo from '../../../assets/tokenize-logo.svg';

export default function TokenizeTitle() {
    return <img width={300} src={logo} alt="Tokenize" />;
}
