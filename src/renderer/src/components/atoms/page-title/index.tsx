import cn from 'classnames';
import { ReactComponent as NftMark } from '../../../assets/nft.svg';
import Skeleton from 'react-loading-skeleton';
import styles from './page-title.module.css';

export default function PageTitle({
    label,
    greyPart,
    isNft,
    isLoading,
}: {
    label: string;
    greyPart?: string;
    isNft?: boolean;
    isLoading?: boolean;
}) {
    const titleClass = cn({
        [styles['page-title']]: true,
        [styles['less-margin-bottom']]: greyPart || isNft,
        [styles['bigger-margin-bottom']]: !greyPart && !isNft,
        [styles['page-title-skeleton']]: isLoading,
    });
    return (
        <>
            {isLoading ? (
                <h2 className={titleClass}>
                    <Skeleton width={260} height={30} />
                    <Skeleton width={100} height={30} />
                </h2>
            ) : (
                <h2 className={titleClass}>
                    {label}
                    {greyPart && (
                        <span className={styles['grey-part']}>{greyPart}</span>
                    )}
                    {isNft && <NftMark />}
                </h2>
            )}
        </>
    );
}
