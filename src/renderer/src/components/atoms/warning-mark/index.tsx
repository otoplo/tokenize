import { ReactComponent as WarningMarkIcon } from '../../../assets/warning-mark.svg';
import styles from './warning-mark.module.css';
export default function WarningMark() {
    return (
        <div className={styles['warning-mark']}>
            <WarningMarkIcon />
        </div>
    );
}
