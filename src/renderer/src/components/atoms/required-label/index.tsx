import styles from './required-labe.module.css';

export default function RequiredLabel({
    label = '*Required',
}: {
    label?: string;
}) {
    return <h4 className={styles['required-label']}>{label}</h4>;
}
