import styles from './textarea-field.module.css';

export default function TextAreaField({
    label,
    onChange,
    value,
    placeholder,
}: {
    label: string;
    onChange?: any;
    value?: string;
    placeholder?: string;
}) {
    return (
        <label className={styles['textarea-field']}>
            {label}:
            <textarea
                placeholder={placeholder || 'Enter...'}
                value={value}
                onChange={onChange}
                rows={4}
                style={{ width: '422px' }}
                disabled
            />
        </label>
    );
}
