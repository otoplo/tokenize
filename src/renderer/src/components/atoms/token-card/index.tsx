import { Link } from 'react-router-dom';
import Skeleton from 'react-loading-skeleton';
import PageTitle from '../page-title';
import PermissionToggle, { PermissionLable, PermissionState } from '../permission-toggle';
import MiniButton from '../mini-button';
import styles from './token-card.module.css';
import { useAppSelector } from '../../../store/hooks';
import { loadingState } from '../../../store/slices/loading';
import dummyLogo from '../../../assets/token-icon-placeholder.svg';
import { TokenEntity } from '@renderer/core/db/entities';
import { parseAmountWithDecimals } from '@renderer/utils/utils';

export interface TokenCardDataDynamic {
    amount: Amount;
    balance: bigint;
    pending: bigint;
    permissions: Permission[];
    authorities: AuthorityData[];
}

export interface Permission {
    label: PermissionLable;
    state: PermissionState;
}

export interface Amount {
    label: string;
    amount: string;
    rawAmount: string | number | bigint;
}

export interface AuthorityData {
    authorise: boolean;
    melt: boolean;
    mint: boolean;
    rescript: boolean;
    subgroup: boolean;
    outpoint: string;
}

export interface TX {
    action: string;
    height: number;
    timestamp: number;
    amount: string;
    txidem: string;
}

export default function TokenCard({
    tokenInfo,
    isNft,
    tokenExtendedInfo,
    handleClickToggle,
    isImportTokenPage,
}: {
    tokenInfo: TokenEntity;
    isNft: boolean;
    tokenExtendedInfo?: TokenCardDataDynamic;
    handleClickToggle: (label: string) => void;
    isImportTokenPage?: boolean;
}) {
    const isLoading = useAppSelector(loadingState);

    const renderLogo = () => {
        if (isLoading) {
            return (
                <Skeleton
                    containerClassName={styles['logo-skeleton']}
                    circle
                    width={108}
                    height={108}
                />
            );
        }
        return tokenInfo.iconUrl ? <img src={tokenInfo.iconUrl} alt={`${tokenInfo.name} logo`} /> : <img src={dummyLogo} alt={`${tokenInfo.name} logo`} />;
    };

    const renderPermissions = () => {
        if (isLoading) {
            return Array(5)
                .fill('')
                .map((_, index) => (
                    <Skeleton
                        key={index}
                        width={110}
                        height={30}
                        borderRadius={15}
                    />
                ));
        }
        return tokenExtendedInfo?.permissions.map((el: any, index) => (
            <PermissionToggle
                state={
                    isImportTokenPage
                        ? el.state === 'active'
                            ? 'note'
                            : el.state
                        : el.state
                }
                key={index}
                label={el.label}
                onClick={() => handleClickToggle(el.label)}
            />
        ));
    };

    return (
        <div className={styles['token-card']}>
            <div className={styles['token-card-header']}>
                {renderLogo()}
                <PageTitle
                    label={tokenInfo.name}
                    greyPart={tokenInfo.ticker}
                    isNft={isNft}
                    isLoading={isLoading}
                />
                <div className={styles['address-section']}>
                    <p className={styles['address']}>
                        {isLoading ? (
                            <Skeleton width={400} height={20} />
                        ) : (
                            <span>{tokenInfo.token}</span>
                        )}
                    </p>
                    {!isLoading && !isImportTokenPage && (
                        <>
                            <MiniButton
                                url={tokenInfo.token}
                                type={'qr-hint'}
                                isTokenCard={true}
                            />
                            <MiniButton text={tokenInfo.token} type={'copy'} />
                        </>
                    )}
                </div>
            </div>
            <div className={styles['token-card-body']}>
                { isImportTokenPage ||
                    <div className={styles['permissions']}>
                        <h6 className={styles['label']}>Permissions:</h6>
                        <div>{renderPermissions()}</div>
                    </div> 
                }
                <div className={styles['description']}>
                    <h6 className={styles['label']}>Description:</h6>
                    <p>
                        {isLoading ? (
                            <Skeleton width={580} height={40} />
                        ) : (
                            <span>{tokenInfo.summary}</span>
                        )}
                    </p>
                </div>
                <div className={styles['metadata']}>
                    <h6 className={styles['label']}>Metadata:</h6>
                    {isLoading ? (
                        <Skeleton width={387} height={15} />
                    ) : (
                        <Link
                            to={tokenInfo.docUrl}
                            target="_blank"
                            className={styles['metadata-link']}
                        >
                            {tokenInfo.docUrl}
                        </Link>
                    )}
                </div>
                { tokenExtendedInfo?.amount && tokenExtendedInfo?.amount.amount !== 'unknown' &&
                    <div className={styles['amount-label']}>
                        <h6 className={styles['label']}>
                            {tokenExtendedInfo.amount.label}:
                        </h6>
                        {isLoading ? (
                            <Skeleton width={387} height={15} />
                        ) : (
                            <p>{tokenExtendedInfo.amount.amount}</p>
                        )}
                    </div>
                }
                {!isImportTokenPage && (tokenExtendedInfo?.balance !== undefined || tokenExtendedInfo?.pending) && !isLoading ? (
                    <div className={styles['balance-label']}>
                        <h6 className={styles['label']}>Balance:</h6>
                        {parseAmountWithDecimals(tokenExtendedInfo.balance, tokenInfo.decimals)}
                        { tokenExtendedInfo?.pending ? ` (Pending: ${parseAmountWithDecimals(tokenExtendedInfo.pending, tokenInfo.decimals)})` : '' }
                    </div>
                ) : (
                    <>
                        {isLoading && (
                            <div className={styles['balance-label']}>
                                <h6 className={styles['label']}>Balance:</h6>
                                <Skeleton width={387} height={15} />
                            </div>
                        )}
                    </>
                )}
                { isImportTokenPage || isLoading ||
                    <div className={styles['permissions']}>
                        <h6 className={styles['label']}>Actions:</h6>
                        <div>
                            { isNft || <PermissionToggle label='Send' state='active' onClick={() => handleClickToggle('send')}/> }
                            <PermissionToggle label='Receive' state='active' onClick={() => handleClickToggle('receive')}/>
                        </div>
                    </div> 
                }
            </div>
        </div>
    );
}
