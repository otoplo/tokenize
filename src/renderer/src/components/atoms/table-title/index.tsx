import styles from './table-title.module.css';

export default function TableTitle({ label }: { label: string }) {
    return <h3 className={styles['table-title']}>{label}</h3>;
}
