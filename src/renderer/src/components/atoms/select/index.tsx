import { useState } from 'react';
import { ReactComponent as DropdownArrow } from '../../../assets/dropdown-arrow.svg';
import { useCloseByClickOutside } from '../../../hooks/useCloseByClickOutSide';
import styles from './select.module.css';
import { truncateStringMiddle } from '../../../utils/utils';

export interface Option {
    img: string;
    name: string;
    tokenId: string;
}
interface InterfaceSelect {
    label: string;
    placeholder?: string;
    options?: Option[];
    value?: string;
    onSelect: (coin: string) => void;
}

export default function Select({
    placeholder = 'Select option...',
    label,
    options,
    onSelect,
    value,
}: InterfaceSelect) {
    const [isOpen, setIsOpen] = useState<boolean>(false);

    const handleToggle = (e: { preventDefault: () => void }) => {
        e.preventDefault();
        setIsOpen((prev) => !prev);
    };

    const handleSelect = (el: Option) => {
        onSelect(el.name);
        setIsOpen(false);
    };

    const onClose = () => {
        setIsOpen(false);
    };

    const ListOfOptions = useCloseByClickOutside(({ onClose }) => {
        return (
            <ul className={styles['options-list']}>
                {options?.map((el, index) => (
                    <li
                        key={index}
                        className={styles['option']}
                        onClick={() => handleSelect(el)}
                    >
                        <img src={el.img} style={{ width: '30px' }} />
                        <span>{el.name}</span>
                        {truncateStringMiddle(el.tokenId, 15)}
                    </li>
                ))}
            </ul>
        );
    });

    return (
        <div className={styles['select']}>
            <div
                className={styles['select-body']}
                style={{ pointerEvents: isOpen ? 'none' : 'auto' }}
            >
                <h6 onClick={handleToggle} className={styles['label']}>
                    {label}:
                </h6>
                <div className={styles['select-button']} onClick={handleToggle}>
                    <>
                        {value ? (
                            <span className={styles['value']}>{value}</span>
                        ) : (
                            <span className={styles['placeholder']}>
                                {placeholder}
                            </span>
                        )}
                        {isOpen ? (
                            <DropdownArrow style={{ rotate: '180deg' }} />
                        ) : (
                            <DropdownArrow />
                        )}
                    </>
                </div>
            </div>
            {isOpen && <ListOfOptions onClose={onClose} />}
        </div>
    );
}
