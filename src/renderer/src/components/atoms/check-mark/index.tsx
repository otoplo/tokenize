import cn from 'classnames';
import WarningMark from '../warning-mark';
import Skeleton from 'react-loading-skeleton';
import styles from './check-mark.module.css';

interface InterfaceCheckMark {
    warning?: boolean;
    isActive?: boolean;
    isLoading?: boolean;
}

export default function CheckMark({
    warning,
    isActive = true,
    isLoading = false,
}: InterfaceCheckMark) {
    const checkMarkClass = cn({
        [styles['check-mark']]: true,
        [styles['check-mark-deactivated']]: !isActive,
    });
    return (
        <>
            {isLoading ? (
                <Skeleton circle width={26} height={26} />
            ) : (
                <div className={checkMarkClass}>
                    {warning && <WarningMark />}
                </div>
            )}
        </>
    );
}
