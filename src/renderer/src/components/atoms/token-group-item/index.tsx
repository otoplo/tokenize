import cn from 'classnames';
import { NavLink, useLocation } from 'react-router-dom';
import { ReactComponent as NftMark } from '../../../assets/nft.svg';
import styles from './token-group-item.module.css';
import { InterfaceSimpleTokenGroupItem } from '@renderer/components/molecules/token-groups';
import dummyLogo from '../../../assets/token-icon-placeholder.svg'

interface InterfaceTokenGroupItem extends InterfaceSimpleTokenGroupItem {
    isParent?: boolean;
    isChild?: boolean;
    isLastChild?: boolean;
}

export default function TokenGroupItem({
    image,
    name,
    shortName,
    description,
    isParent = false,
    isChild = false,
    isLastChild = false,
    isNFT = false,
    tokenId,
}: InterfaceTokenGroupItem) {
    const location = useLocation();
    const TokenGroupClass = cn({
        [styles['token-group-item']]: true,
        [styles['token-group-item-active']]:
            location.pathname === '/token-group-page/' + tokenId,
        [styles['token-group-item-prev-active']]:
            location.pathname === '/token-group-page/' + (tokenId + 1),
        [styles['token-group-parent']]: isParent,
        [styles['token-group-child']]: isChild,
        [styles['token-group-last-child']]: isLastChild,
    });
    return (
        <NavLink to={`/token-group-page/${tokenId}`}>
            <div className={TokenGroupClass}>
                {isNFT && (
                    <div className={styles['nft-mark-wrapper']}>
                        <NftMark />
                    </div>
                )}
                {isParent || isChild || isLastChild ? (
                    <div className={styles['decoration']} />
                ) : null}
                <img src={image || dummyLogo} alt={`${name} logo`} />
                <div>
                    <h3 className={styles['team-title']}>
                        <span className={styles['team-name']}>{name}</span>
                        <span className={styles['short-team-name']}>
                            {shortName}
                        </span>
                    </h3>
                    <p className={styles['description']}>{description}</p>
                </div>
            </div>
        </NavLink>
    );
}
