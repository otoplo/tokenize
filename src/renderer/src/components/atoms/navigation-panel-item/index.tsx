import cn from 'classnames';
import WhiteArrow from '../../../assets/white-arrow.svg';
import styles from './navigation-panel-item.module.css';

interface InterfaceActionsPanelItem {
    label: string;
    isActive: boolean;
}

export default function NavigationPanelItem({
    label,
    isActive,
}: InterfaceActionsPanelItem) {
    const NavigationPanelItemClass = cn({
        [styles['navigation-panel-item']]: true,
        [styles['active']]: isActive,
    });
    return (
        <div className={NavigationPanelItemClass}>
            <span>{label}</span>
            <img
                src={WhiteArrow}
                alt="white arrow icon"
                className={styles['navigation-icon']}
            />
        </div>
    );
}
