import cn from 'classnames';
import styles from './simple-button.module.css';
import Skeleton from 'react-loading-skeleton';

interface InterfaceSimpleButton {
    label: string;
    onClick?: () => void;
    type: 'colored' | 'border' | 'text-only';
    isRectangle?: boolean;
    color: 'primary' | 'kill' | 'mint';
    isSubmit?: boolean;
    isLoading?: boolean;
}

export default function SimpleButton({
    label,
    onClick,
    type,
    isRectangle,
    color,
    isSubmit = false,
    isLoading = false,
}: InterfaceSimpleButton) {
    const buttonClass = cn({
        [styles['simple-button']]: true,
        [styles[color]]: true,
        [styles['rectangle']]: isRectangle,
        [styles[type]]: true,
    });

    if (isLoading) {
        return <Skeleton width={250} height={50} borderRadius={25}/>
    }

    return (
        <button
            type={isSubmit ? 'submit' : 'button'}
            onClick={onClick}
            className={buttonClass}
        >
            {label}
        </button>
    );
}
