import { QRCode } from 'react-qrcode-logo';

interface InterfaceQrCode {
    mainAddr: string;
    logo?: string;
}

export default function QrCode({ mainAddr, logo }: InterfaceQrCode) {
    return (
        <QRCode
            value={mainAddr}
            size={140}
            logoImage={logo}
            logoWidth={25}
            logoPadding={1}
            quietZone={5}
        />
    );
}
