import bigDecimal from 'js-big-decimal';
import styles from './amount-label.module.css';
import Skeleton from 'react-loading-skeleton';

interface InterfaceAmountLabel {
    amount: bigDecimal;
    pending: bigDecimal;
    coin?: string;
    decimals?: number;
    isLoading?: boolean;
}

export default function AmountLabel({
    amount,
    pending,
    coin,
    decimals,
    isLoading
}: InterfaceAmountLabel) {

    let amnt = amount;
    let pndg = pending;
    if (decimals) {
        amnt = amount.divide(new bigDecimal(Math.pow(10, decimals)), decimals);
        pndg = pending.divide(new bigDecimal(Math.pow(10, decimals)), decimals);
    }

    let hasPending = pending.compareTo(new bigDecimal(0)) != 0;

    return (
        <>
            <div className={styles['amount-label']}>
                <span className={styles['label']}>{'Balance'}:</span>
                { isLoading ? (
                    <Skeleton width={300} height={50}/>
                ) : (
                    `${amnt.getPrettyValue()} ${coin}`
                ) }
            </div>
            { !isLoading && hasPending &&
                <div className={styles['amount-pending-label']}>
                    <span className={styles['label']}>{'Pending'}: {`${pndg.getPrettyValue()} ${coin}`}</span>
                </div>
            }
        </>
    );
}
