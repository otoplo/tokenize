import styles from './table-header.module.css';

interface InterfaceTableHeader {
    headers: string[];
}

export default function TableHeader({ headers }: InterfaceTableHeader) {
    return (
        <thead className={styles['thead']}>
            <tr>
                {headers.map((header, index) => (
                    <th key={index}>{header}</th>
                ))}
            </tr>
        </thead>
    );
}
