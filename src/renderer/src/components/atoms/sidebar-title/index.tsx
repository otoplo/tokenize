import styles from './sidebar-title.module.css';

interface InterfaceSidebarTitle {
    label: string;
}

export default function SidebarTitle({ label }: InterfaceSidebarTitle) {
    return <h2 className={styles['sidebar-title']}>{label}</h2>;
}
