import NavigationPanel from '../../molecules/navigation-panel';
import Logo from '../../atoms/logo';
import styles from './sidebar.module.css';
import TokenGroups from '../../molecules/token-groups';

export default function Sidebar() {
    return (
        <div className={styles['sidebar']}>
            <div className={styles['logo-wrapper']}>
                <Logo />
            </div>
            <NavigationPanel />
            <TokenGroups />
        </div>
    );
}
