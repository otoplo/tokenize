import { SetStateAction } from 'react';
import TableHeader from '../../atoms/table-header';
import TableBody from '../../molecules/table-body';
import styles from './table.module.css';
import { AuthorityData } from '@renderer/components/atoms/token-card';

export default function Table({
    headers,
    data,
    tableType,
    onKill,
}: {
    headers: string[];
    data?: any;
    tableType: 'history' | 'authority' | 'transactions' | 'kill_authority';
    tableLabel?: string;
    onKill?: (data: SetStateAction<AuthorityData>) => void;
}) {
    return (
        <table className={styles['table']}>
            <TableHeader headers={headers} />
            <TableBody data={data} tableType={tableType} onKill={onKill} />
        </table>
    );
}
