import Skeleton from 'react-loading-skeleton';
import MiniButton from '../../atoms/mini-button';
import TableTitle from '../../atoms/table-title';
import styles from './receive-section.module.css';
import { useAppSelector } from '../../../store/hooks';
import { walletState } from '../../../store/slices/wallet';
import { truncateStringMiddle } from '../../../utils/utils';

export default function ReceiveSection() {
    const wallet = useAppSelector(walletState);
    const address = wallet.keys.receiveKeys.at(-1)?.address ?? '';
    const isLoading = wallet.initLoading;

    const truncateAddress = truncateStringMiddle(address, 55);
    return (
        <div className={styles['receive-section']}>
            <TableTitle label="Receive" />
            <div className={styles['receive-content']}>
                { isLoading ? (
                    <Skeleton width={500} height={25}/>
                ) : (
                    <>
                        <MiniButton url={address} type={'qr-hint'} />
                        <MiniButton text={address} type={'copy'} />
                        <span className={styles['receive-address']}>
                            {truncateAddress}
                        </span>
                    </>
                )}
            </div>
        </div>
    );
}
