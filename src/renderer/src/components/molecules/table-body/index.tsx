import { SetStateAction } from 'react';
import {
    TableRowHistory,
    TableRowAuthority,
    TableRowTransactions,
    TableRowKillAuthority,
} from '../../atoms/table-row';
import TableRowsSkeleton from '../../atoms/table-row/table-rows-skeleton';
import styles from './table-body.module.css';
import { useAppSelector } from '../../../store/hooks';
import { loadingState } from '../../../store/slices/loading';
import { AuthorityData } from '@renderer/components/atoms/token-card';

function getRowCount(tableType: string): number {
    switch (tableType) {
        case 'history':
            return 14;
        case 'authority':
            return 4;
        case 'transactions':
            return 4;
        case 'kill_authority':
            return 3;
        default:
            return 0;
    }
}

export default function TableBody({
    data,
    tableType,
    onKill,
}: {
    data?: any;
    tableType: 'history' | 'authority' | 'transactions' | 'kill_authority';
    onKill?: (data: SetStateAction<AuthorityData>) => void;
}) {
    const isLoading = useAppSelector(loadingState);
    const rowsCount = getRowCount(tableType);
    return (
        <tbody className={styles['tbody']}>
            {isLoading ? (
                <TableRowsSkeleton rows={rowsCount} tableType={tableType} />
            ) : (
                <>
                    {data?.map((row: any, index: number) => {
                        switch (tableType) {
                            case 'history':
                                return (
                                    <TableRowHistory
                                        key={index}
                                        rowData={row}
                                    />
                                );
                            case 'authority':
                                return (
                                    <TableRowAuthority
                                        key={index}
                                        rowData={row}
                                        onKill={onKill}
                                    />
                                );
                            case 'transactions':
                                return (
                                    <TableRowTransactions
                                        key={index}
                                        rowData={row}
                                    />
                                );
                            case 'kill_authority':
                                return (
                                    <TableRowKillAuthority
                                        key={index}
                                        rowData={row}
                                    />
                                );
                            default:
                                return null;
                        }
                    })}
                </>
            )}
        </tbody>
    );
}
