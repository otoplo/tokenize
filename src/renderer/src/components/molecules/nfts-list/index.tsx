import { Fragment, useState } from 'react';
import { Navigation } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import TableTitle from '../../atoms/table-title';
import NftCard from '../../atoms/nfts-card';
import 'swiper/css';
import 'swiper/css/navigation';
import styles from './nfts-list.module.css';
import { useAppSelector } from '../../../store/hooks';
import { loadingState } from '../../../store/slices/loading';
import { NftEntity } from '@renderer/core/db/entities';
import Popup from '@renderer/components/organism/popup';
import { FullNftPopup } from '../popups';

export default function NftsList({
    nftsList,
}: {
    nftsList?: NftEntity[];
}) {
    const isLoading = useAppSelector(loadingState);
    const [showNFT, setShowNFT] = useState(false);
    const [selectedNFT, setSelectedNFT] = useState<NftEntity>();

    const showNftPopup = (entity: NftEntity) => {
        setSelectedNFT(entity);
        setShowNFT(true);
    }

    return (
        <div className={styles['nfts-list-container']}>
            <TableTitle label={'NFTs' + ` (${nftsList?.length ?? 0})`} />
            <div className={styles['nfts-list']}>
                <button className="my-prev-button" disabled={isLoading} />
                {isLoading ? (
                    <div className={styles['nfts-list-skeleton']}>
                        {Array(9)
                            .fill('')
                            .map((_, index) => (
                                <Fragment key={index}>
                                    <NftCard isLoading={true} />
                                </Fragment>
                            ))}
                    </div>
                ) : (
                    <Swiper
                        spaceBetween={20}
                        slidesPerView={(nftsList?.length ?? 0) > 4 ? 4 : nftsList?.length}
                        slidesPerGroup={2}
                        autoplay={true}
                        modules={[Navigation]}
                        scrollbar={{ draggable: true }}
                        pagination={{ clickable: true }}
                        className={styles['swiper']}
                        breakpoints={{
                            1920: {
                                slidesPerView: (nftsList?.length ?? 0) > 8 ? 8 : nftsList?.length,
                            },
                            1750: {
                                slidesPerView: (nftsList?.length ?? 0) > 7 ? 7 : nftsList?.length,
                                slidesPerGroup: 4,
                            },
                            1650: {
                                slidesPerView: (nftsList?.length ?? 0) > 6 ? 6 : nftsList?.length,
                                slidesPerGroup: 3,
                            },
                            1330: {
                                slidesPerView: (nftsList?.length ?? 0) > 5 ? 5 : nftsList?.length,
                            },
                        }}
                        navigation={{
                            prevEl: '.my-prev-button',
                            nextEl: '.my-next-button',
                        }}
                    >
                        {nftsList?.map((el, index) => (
                            <SwiperSlide key={index}>
                                <NftCard showNft={() => showNftPopup(el)} nftEntity={el} nftNumber={index+1}/>
                            </SwiperSlide>
                        ))}
                    </Swiper>
                )}
                <button className="my-next-button" disabled={isLoading} />
            </div>
            { showNFT &&
                <Popup close={() => setShowNFT(false)}>
                    <FullNftPopup nftEntity={selectedNFT} close={() => setShowNFT(false)}/>
                </Popup>
            }
        </div>
    );
}
