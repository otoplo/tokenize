import { ChangeEvent, Dispatch, SetStateAction } from 'react';
import InputField from '../../atoms/input-field';

export interface InterfaceInput {
    label: string;
    value: string | number;
    required?: boolean;
    id: string;
}

interface InterfaceInputsList {
    inputsValues: InterfaceInput[];
    setInputsValues: Dispatch<SetStateAction<InterfaceInput[]>>;
    inputType?: string;
}

export default function InputsList({
    inputsValues,
    setInputsValues,
    inputType,
}: InterfaceInputsList) {
    const handleInputsValues = (id: string, value: string) => {
        setInputsValues((prevState) => {
            const updatedInputs = prevState.map((input) => {
                if (input.id === id) {
                    if (id === 'decimals' || id === 'quantity') {
                        const numericValue = value.replace(/\D/g, '') || 0;
                        return {
                            ...input,
                            value: Number(numericValue),
                        };
                    }
                    return {
                        ...input,
                        value,
                    };
                }
                return input;
            });
            return updatedInputs;
        });
    };

    return (
        <>
            {inputsValues?.map((el) => (
                <InputField
                    key={el.id}
                    label={el.label}
                    size={'long'}
                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                        handleInputsValues(el.id, e.target.value)
                    }
                    required={el.required}
                    inputType={
                        el.id === 'decimals' || el.id === 'quantity'
                            ? 'number'
                            : inputType
                    }
                    value={el.value.toString()}
                />
            ))}
        </>
    );
}
