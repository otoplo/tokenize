import { useEffect, useState } from 'react';
import TokenGroupItem from '../../atoms/token-group-item';
import SidebarTitle from '../../atoms/sidebar-title';
import styles from './token-groups.module.css';
import { useLiveQuery } from 'dexie-react-hooks';
import { tokensRepository } from '@renderer/core/db/tokens.repository';
import NiftyProvider from '@renderer/providers/niftyProvider';

interface InterfaceTokenGroup {
    tokenId: string;
    image: string;
    name: string;
    shortName: string;
    description: string;
    isNFT?: boolean;
    children?: InterfaceSimpleTokenGroupItem[];
}

export interface InterfaceSimpleTokenGroupItem {
    tokenId: string;
    image: string;
    name: string;
    shortName: string;
    description: string;
    isNFT?: boolean;
}

export default function TokenGroups() {
    const parents = useLiveQuery(() => tokensRepository.findAllParents());
    const subgroups = useLiveQuery(() => tokensRepository.findAllSubgroups());

    const [data, setData] = useState<InterfaceTokenGroup[]>();

    useEffect(() => {
        let data = parents?.map((group) => {
            let groupToken: InterfaceTokenGroup = {
                tokenId: group.token,
                shortName: group.ticker,
                name: group.name,
                description: group.summary,
                image: group.iconUrl,
                isNFT: group.tokenIdHex === NiftyProvider.NIFTY_TOKEN.tokenIdHex,
            };
            if (!groupToken.isNFT) {
                let childes = subgroups?.filter(sg => sg.parentGroup === group.token).map(subgroup => {
                    let child: InterfaceSimpleTokenGroupItem = {
                        tokenId: subgroup.token,
                        shortName: subgroup.ticker,
                        name: subgroup.name,
                        description: subgroup.summary,
                        image: subgroup.iconUrl,
                    };
                    return child;
                })
                groupToken.children = childes;
            }
            return groupToken;
        });
        
        setData(data)
    }, [parents, subgroups]);

    const renderItem = (
        data: InterfaceTokenGroup,
        kind?: 'isParent' | 'isChild' | 'isLastChild',
    ) => {
        return (
            <TokenGroupItem
                key={data.tokenId}
                image={data.image}
                name={data.name}
                shortName={data.shortName}
                description={data.description}
                isNFT={data.isNFT}
                isParent={kind === 'isParent'}
                isChild={kind === 'isChild'}
                isLastChild={kind === 'isLastChild'}
                tokenId={data.tokenId}
            />
        );
    };

    const getKind = (index: number, length: number) => {
        if (index === length - 1) {
            return 'isLastChild';
        }
        return 'isChild';
    };

    const renderChilds = (data: InterfaceTokenGroup[], length: number) => {
        return data.map((el, index) => {
            const kind = getKind(index, length);
            return renderItem(el, kind);
        });
    };

    const renderParent = (data: InterfaceTokenGroup) => {
        return (
            <div key={data.tokenId}>
                {renderItem(data, 'isParent')}
                {renderChilds(data.children || [], data.children?.length || 0)}
            </div>
        );
    };

    return (
        <div className={styles['token-groups-wrapper']}>
            <SidebarTitle label="Token Groups" />
            <div className={styles['token-group-list']}>
                {data && data.length > 0 &&
                    data.map((el: InterfaceTokenGroup) =>
                        el.children?.length && el.children?.length > 0
                            ? renderParent(el)
                            : renderItem(el),
                    )}
            </div>
        </div>
    );
}
