import { ChangeEvent, FormEvent, ReactElement, useState } from 'react';
import InputField from '../../atoms/input-field';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';
import { MAX_INT64, getRawAmount, isNullOrEmpty, parseAmountWithDecimals } from '@renderer/utils/utils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';
import { buildAndSignMintTransaction } from '@renderer/core/wallet/txUtils';
import { useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import bigDecimal from 'js-big-decimal';
import SendConfirmationPopup from './send-confirmation-popup';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';

export default function MintPopup({
    token,
    ticker,
    currentSupply,
    decimals,
    close,
}: {
    token: string;
    ticker: string;
    currentSupply: string;
    decimals: number;
    close: () => void;
}) {
    const [mintAmount, setMintAmount] = useState('');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');
    const [loading, setLoading] = useState(false);

    const [showPopUp, setShowPopUp] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [tx, setTx] = useState<Transaction>();

    const amountRegx1 = decimals ? new RegExp(`^0\.[0-9]{1,${decimals}}$`) : /^[1-9][0-9]*$/;
    const amountRegx2 = decimals ? new RegExp(`^[1-9][0-9]*(\.[0-9]{1,${decimals}})?$`) : /^[1-9][0-9]*$/;

    const wallet = useAppSelector(walletState);

    const handleChangeSupply = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.value === '0' || amountRegx1.test(e.target.value) || amountRegx2.test(e.target.value)) {
            setMintAmount(e.target.value)
        } else if (isNullOrEmpty(e.target.value)) {
            setMintAmount('');
        }
    };

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            if (!amountRegx1.test(mintAmount) && !amountRegx2.test(mintAmount)) {
                throw new Error("Invalid Amount.");
            }

            let rawAmount = getRawAmount(mintAmount, decimals);
            if (BigInt(rawAmount) > MAX_INT64) {
                throw new Error(`Cannot Mint more than ${parseAmountWithDecimals(MAX_INT64, decimals)} in single transaction`);
            }

            setLoading(true);
            let tx = await buildAndSignMintTransaction(wallet.keys, rawAmount, token);

            setTx(tx);
            setShowPopUp(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
            setShowAlert(true);
        } finally {
            setLoading(false);
        }
    };

    const confirmTx = (idem: string) => {
        setShowPopUp(false);
        setAlertMessage(<><div>Transaction sent succesfully.</div><br/><div>IDEM:</div><div style={{wordBreak: 'break-all'}}>{idem}</div></>);
        setShowSuccess(true);
    }

    const closeSuccess = () => {
        close();
    }

    return (
        <div className={styles['change-supply-popup']}>
            <PageTitle label={'Change Supply'} />
            <div className={styles['change-supply-popup-content']}>
                <p>
                    Select the amount of tokens you would like to mint.
                </p>
                <form onSubmit={handleSubmit}>
                    {currentSupply && (
                        <div className={styles['current-supply']}>
                            <h6 className={styles['label']}>Current Supply:</h6>
                            <p>{parseAmountWithDecimals(currentSupply, decimals)}</p>
                        </div>
                    )}
                    <InputField
                        label={'Mint Amount'}
                        size={'long'}
                        value={mintAmount}
                        required
                        onChange={handleChangeSupply}
                        inputType={'number'}
                        step="any"
                    />
                    <div className={styles['control-box']}>
                        <SimpleButton
                            label={'Mint'}
                            type={'colored'}
                            color={'mint'}
                            isSubmit={true}
                            isLoading={loading}
                        />
                    </div>
                </form>
            </div>
            { showPopUp &&
                <Popup close={() => setShowPopUp(false)} title={'Minting Confirmation'}>
                    <SendConfirmationPopup
                        token={token}
                        ticker={ticker}
                        amount={new bigDecimal(mintAmount).getPrettyValue()}
                        rawAmount={getRawAmount(mintAmount, decimals)}
                        tx={tx}
                        txType={TxTokenType.MINT}
                        confirm={confirmTx}
                    />
                </Popup>
            }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup isMini close={closeSuccess}>
                    <AlertPopup label='Confirmed' message={alertMessage} close={closeSuccess}/>
                </Popup>
            }
        </div>
    );
}
