import { ChangeEvent, FormEvent, useState } from 'react';
import InputField from '../../atoms/input-field';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import TextareaField from '../../atoms/textarea-field';
import styles from './popups.module.css';

export default function MintNftPopup() {
    const [name, setName] = useState<string>('');
    const [metaData, setMetaData] = useState<string>('');

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        console.log({ name, metaData });
    };

    const handleInputName = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e?.target.value);
    };

    const handleInputMetadata = (e: ChangeEvent<HTMLInputElement>) => {
        setMetaData(e?.target.value);
    };

    return (
        <div className={styles['mint-nft-popup']}>
            <PageTitle label={'Mint NFT'} />
            <div className={styles['mint-nft-popup-content']}>
                <p>Enter your NFT details here.</p>
                <form onSubmit={handleSubmit}>
                    <InputField
                        label={'NFT Name'}
                        size={'long'}
                        onChange={handleInputName}
                    />
                    <TextareaField
                        label={'Metadata'}
                        value={metaData}
                        onChange={handleInputMetadata}
                    />
                    <SimpleButton
                        label={'Mint'}
                        type={'colored'}
                        color={'primary'}
                        isSubmit={true}
                    />
                </form>
            </div>
        </div>
    );
}
