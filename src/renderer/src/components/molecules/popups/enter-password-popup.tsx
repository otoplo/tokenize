import { ChangeEvent, useState } from 'react';
import InputField from '../../atoms/input-field';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';
import { isPasswordValid } from '../../../core/wallet/seedUtils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';

export default function EnterPasswordPopup({
    AuthorizedOn,
}: {
    AuthorizedOn: (mnemonic: string) => void;
}) {
    const [password, setPassword] = useState<string>('');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');

    const handleClickConfirm = () => {
        if (password) {
            let decMn = isPasswordValid(password);
            if (decMn) {
                AuthorizedOn(decMn);
            } else {
                setAlertMessage("Wrond password.");
                setShowAlert(true);
            }
        }
    };

    const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };

    return (
        <>
            <div className={styles['enter-password-popup']}>
                <PageTitle label={'Enter Password'} />
                <div className={styles['enter-password-popup-content']}>
                    <p>Enter the password that was used to decrypt your wallet.</p>
                    <InputField
                        label={'Password'}
                        size={'short'}
                        onChange={handlePasswordChange}
                        inputType={'password'}
                    />
                </div>
                <SimpleButton
                    label={'Confirm'}
                    type={'colored'}
                    color={'primary'}
                    onClick={handleClickConfirm}
                />
            </div>
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Failed' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
        </>
    );
}
