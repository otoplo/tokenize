import InputField from '@renderer/components/atoms/input-field';
import styles from './popups.module.css';
import SimpleButton from '@renderer/components/atoms/simple-button';
import { ChangeEvent, FormEvent, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';
import { RostrumProvider } from '@renderer/providers/rostrumProvider';
import StorageProvider from '@renderer/providers/storageProvider';
import { RostrumTransportScheme } from '@renderer/core/models/rostrumParams';

export default function EditRostrumPopup({
    scheme,
    host,
    port,
}: {
    scheme: string;
    host: string;
    port: string;
}) {

    const [rostrumScheme, setRostrumScheme] = useState(scheme);
    const [rostrumHost, setRostrumHost] = useState(host);
    const [rostrumPort, setRostrumPort] = useState(port);
    const [loading, setLoading] = useState(false);

    const [showSuccess, setShowSuccess] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<string>('');

    const handleChangeScheme = (e: ChangeEvent<HTMLInputElement>) => {
        setRostrumScheme(e.target.value);
    };

    const handleChangeHost = (e: ChangeEvent<HTMLInputElement>) => {
        setRostrumHost(e.target.value);
    };

    const handleChangePort = (e: ChangeEvent<HTMLInputElement>) => {
        setRostrumPort(e.target.value);
    };

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        let newRostrum: RostrumProvider | undefined = undefined;
        try {
            setLoading(true);

            if (rostrumScheme !== 'ws' && rostrumScheme !== 'wss') {
                throw new Error("Scheme must be 'ws' or 'wss'");
            }

            if (!/^\d+$/.test(rostrumPort)) {
                throw new Error("Port is not valid");
            }

            try {
                newRostrum = new RostrumProvider({host: rostrumHost, port: parseInt(rostrumPort), scheme: rostrumScheme});
                await newRostrum.connect();
            } catch {
                throw new Error('Unable to connect to new rostrum instance');
            }
            
            let version = await newRostrum.getVersion();
            let major = parseInt(version[0].split(' ')[1].split('.')[0]);
            if (major < 10) {
                throw new Error('Tokenize must work with Rostum version 10 or higher');
            }
            setAlertMessage("After confirmation you will be redirected to home page to login again.")
            setShowSuccess(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : 'An error occured, try again later.');
            setShowAlert(true);
        } finally {
            if (newRostrum) {
                await newRostrum.disconnect();
            }
            setLoading(false);
        }
    }

    const confirmEdit = () => {
        StorageProvider.saveRostrumParams({host: rostrumHost, port: parseInt(rostrumPort), scheme: (rostrumScheme as RostrumTransportScheme)});
        window.location.reload();
    }

    return (
        <div className={styles['edit-rostrum-popup']}>
            <div className={styles['edit-rostrum-content']}>
                <form onSubmit={handleSubmit}>
                    <InputField
                        label={'Scheme'}
                        size={'short'}
                        value={rostrumScheme}
                        placeholder={"'ws' or 'wss' only"}
                        onChange={handleChangeScheme}
                    />
                    <InputField
                        label={'Host'}
                        size={'short'}
                        value={rostrumHost}
                        placeholder={"e.g 'electrum.nexa.org'"}
                        onChange={handleChangeHost}
                    />
                    <InputField
                        label={'Port'}
                        inputType={'number'}
                        size={'short'}
                        value={rostrumPort}
                        onChange={handleChangePort}
                    />
                    { loading ? (
                        <span className={styles['skeleton']}>
                            <Skeleton width={250} height={50} borderRadius={25}/>
                        </span>
                    ) : (
                        <SimpleButton
                            label={'Edit'}
                            type={'colored'}
                            color={'primary'}
                            isSubmit={true}
                        />
                    ) }
                </form>
            </div>
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup isTiny close={() => setShowSuccess(false)}>
                    <AlertPopup label='Confirmation' message={alertMessage} btnLabel='Confirm' close={confirmEdit}/>
                </Popup>
            }
        </div>
    );
}
