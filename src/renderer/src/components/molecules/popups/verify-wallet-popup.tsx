import { ChangeEvent, FormEvent, useState } from 'react';
import PageTitle from "@renderer/components/atoms/page-title";
import styles from './popups.module.css';
import SimpleButton from "@renderer/components/atoms/simple-button";
import InputField from '@renderer/components/atoms/input-field';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';

const emptyMnemonic = {
    1: '',
    2: '',
    3: '',
    4: '',
    5: '',
    6: '',
    7: '',
    8: '',
    9: '',
    10: '',
    11: '',
    12: '',
};

export default function VerifyWalletPopup({
    mnemonic,
    nextStep,
    prevStep,
}: {
    mnemonic: string[];
    nextStep: () => void;
    prevStep: () => void;
}) {

    const [mnemonicVal, setMnemonicVal] = useState(emptyMnemonic);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');

    const arrayFromObject = Object.entries(mnemonicVal).map(([key, value]) => ({
        [key]: value,
    }));

    const handleInputChange = (key: string, value: string) => {
        setMnemonicVal((prevState) => ({
            ...prevState,
            [key]: value.trim().toLowerCase(),
        }));
    };

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        let vals = Object.values(mnemonicVal);
        for (let i = 1; i < mnemonic.length; i += 2) {
            if (mnemonic[i] !== vals[i]) {
                setAlertMessage("Invalid Seed.");
                setShowAlert(true);
                return;
            }
        }
        
        nextStep();
    };

    return (
        <div className={styles['restore-wallet-popup']}>
            <PageTitle label={'Verify Wallet Seed'} />
            <div className={styles['restore-wallet-popup-content']}>
                <p>
                    <span>
                        Please enter every second word of your seed phrase.
                    </span>
                    <span>
                        Make sure to spell every word correctly and insert them in
                        the correct order.
                    </span>
                </p>
                <form onSubmit={handleSubmit}>
                    {arrayFromObject?.map((el, i) => {
                        return <InputField
                            key={Object.keys(el)[0]}
                            label={Object.keys(el)[0]}
                            size={'short'}
                            dot={true}
                            onChange={(
                                e: ChangeEvent<HTMLInputElement>,
                            ) =>
                                handleInputChange(
                                    Object.keys(el)[0],
                                    e.target.value,
                                )
                            }
                            hidden={i % 2 === 0}
                        />
                    })}
                    <div className={styles['control-box']}>
                        <SimpleButton
                            label={'Back'}
                            type={'colored'}
                            color={'primary'}
                            onClick={prevStep}
                        />
                        <SimpleButton
                            label={'Validate'}
                            type={'colored'}
                            color={'primary'}
                            isSubmit={true}
                        />
                    </div>
                </form>
            </div>
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Failed' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
        </div>
    )
}
