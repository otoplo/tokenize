import PageTitle from '../../atoms/page-title';
import SoonLabel from '../../atoms/soon-label';
import styles from './popups.module.css';

export default function RescriptPopup() {
    return (
        <div className={styles['rescript-popup']}>
            <PageTitle label={'Rescript Tokens'} />
            <SoonLabel />
        </div>
    );
}
