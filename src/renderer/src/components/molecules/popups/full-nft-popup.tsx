import PageTitle from '@renderer/components/atoms/page-title';
import { ChangeEvent, FormEvent, ReactElement, useEffect, useState } from 'react';
import styles from './popups.module.css';
import InputField from '@renderer/components/atoms/input-field';
import SimpleButton from '@renderer/components/atoms/simple-button';
import Popup from '@renderer/components/organism/popup';
import SendConfirmationPopup from './send-confirmation-popup';
import AlertPopup from './alert-popup';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';
import { NftEntity } from '@renderer/core/db/entities';
import JSZip from 'jszip';
import { isNullOrEmpty, truncateStringMiddle } from '@renderer/utils/utils';
import { toast } from 'react-toastify';
import { buildAndSignTransferTransaction } from '@renderer/core/wallet/txUtils';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import { useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import { nftsRepository } from '@renderer/core/db/nfts.repository';

interface NftFileDetails {
    title: string;
    image: string;
}

export default function FullNftPopup({
    nftEntity,
    close
} : {
    nftEntity?: NftEntity;
    close: () => void;
}) {

    const [loading, setLoading] = useState(false);
    const [toAddress, setToAddress] = useState('');

    const [showPopUp, setShowPopUp] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');

    const [tx, setTx] = useState<Transaction>();

    const [title, setTitle] = useState('');
    const [series, setSeries] = useState('');
    const [files, setFiles] = useState<NftFileDetails[]>();

    const wallet = useAppSelector(walletState);

    useEffect(() => {
        async function loadData(zipData: Buffer) {
            let files: NftFileDetails[] = [];

            let zip = await JSZip.loadAsync(zipData);
            let info = zip.file('info.json');
            if (info) {
                let infoJson = await info.async('string');
                let infoObj = JSON.parse(infoJson);
                if (infoObj.title) {
                    setTitle(infoObj.title);
                }
                if (infoObj.series) {
                    setSeries(infoObj.series);
                }
            }

            let pubImg = zip.file(/^public\./);
            if (!isNullOrEmpty(pubImg)) {
                let img = await pubImg[0].async('blob');
                let url = URL.createObjectURL(img);
                files.push({ title: 'Public', image: url })
            }

            let frontImg = zip.file(/^cardf\./);
            if (!isNullOrEmpty(frontImg)) {
                let img = await frontImg[0].async('blob');
                let url = URL.createObjectURL(img);
                files.push({ title: 'Front', image: url })
            }

            let backImg = zip.file(/^cardb\./);
            if (!isNullOrEmpty(backImg)) {
                let img = await backImg[0].async('blob');
                let url = URL.createObjectURL(img);
                files.push({ title: 'Back', image: url })
            }

            let ownImg = zip.file(/^owner\./);
            if (!isNullOrEmpty(ownImg)) {
                let img = await ownImg[0].async('blob');
                let url = URL.createObjectURL(img);
                files.push({ title: 'Owner', image: url })
            }

            setFiles(files);
        }

        if (nftEntity) {
            loadData(nftEntity.zipData).catch(e => {
                console.log(e)
            });
        }
    }, [nftEntity]);

    const handleChangeAddress = (e: ChangeEvent<HTMLInputElement>) => {
        setToAddress(e.target.value);
    };

    const exportNft = async () => {
        try {
            let success = await window.api.exportFile(nftEntity!.zipData, title || nftEntity!.tokenIdHex);
            if (success) {
                toast.success("NFT Saved!", {
                    position: "top-right",
                    autoClose: 1500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "dark",
                });
            }
        } catch (e) {
            toast.error("Failed to export NFT", {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
            console.log(e);
        }
    }

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            setLoading(true);
            let tx = await buildAndSignTransferTransaction(wallet.keys, toAddress, "1", nftEntity!.token);

            setTx(tx);
            setShowPopUp(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
            setShowAlert(true);
        } finally {
            setLoading(false);
        }
    }

    const closeSuccess = () => {
        close();
    }

    const confirmTx = (idem: string) => {
        nftsRepository.delete(nftEntity!.tokenIdHex);
        setShowPopUp(false);
        setAlertMessage(<><div>Transaction sent succesfully.</div><br/><div>IDEM:</div><div style={{wordBreak: 'break-all'}}>{idem}</div></>);
        setShowSuccess(true);
    }

    return (
        <div className={styles['full-nft-popup']}>
            <PageTitle label={'NFT Details'} />
            <div className={styles['full-nft-popup-content']}>
                <div className={styles['nft-cards']}>
                    { files?.map((f, i) => (
                        <div className={styles['nft-card']} key={i}>
                            <img src={f.image} alt={`${f.title}`} />
                            <div className={styles['nft-box']}>
                                <p>{f.title}</p>
                            </div>
                        </div>
                    ))}
                </div>
                <div className={styles['nft-content-details']}>
                    <p>
                        <span>NFT ID:</span>
                        <span className={styles['content-break']}>{truncateStringMiddle(nftEntity?.token, 55)}</span>
                    </p>
                    <p>
                        <span>Title:</span>
                        {title}
                    </p>
                    <p>
                        <span>Series:</span>
                        {series}
                    </p>
                </div>
                <form onSubmit={handleSubmit}>
                    <InputField
                        label={'Address'}
                        size={'long'}
                        value={toAddress}
                        address={true}
                        required
                        onChange={handleChangeAddress}
                        setPaste={setToAddress}
                    />
                    <div className={styles['control-box']}>
                        <SimpleButton
                            label={'Export'}
                            type={'colored'}
                            color={'primary'}
                            onClick={exportNft}
                            isLoading={loading}
                        />
                        <SimpleButton
                            label={'Send'}
                            type={'colored'}
                            color={'primary'}
                            isSubmit={true}
                            isLoading={loading}
                        />
                    </div>
                </form>
            </div>
            { showPopUp &&
                <Popup close={() => setShowPopUp(false)} title={'Send NFT Confirmation'}>
                    <SendConfirmationPopup
                        token={nftEntity?.token}
                        ticker={''}
                        destination={toAddress}
                        amount={"1"}
                        rawAmount={"1"}
                        tx={tx}
                        txType={TxTokenType.TRANSFER}
                        confirm={confirmTx}
                    />
                </Popup>
            }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup isMini close={closeSuccess}>
                    <AlertPopup label='Confirmed' message={alertMessage} close={closeSuccess}/>
                </Popup>
            }
        </div>
    )
}
