import { useEffect, useState } from 'react';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';
import { generateMnemonicWords, joinMnemonicValue } from '../../../core/wallet/seedUtils';
import EncryptWalletPopup from './encrypt-wallet-popup';
import VerifyWalletPopup from './verify-wallet-popup';

export default function CreateWalletPopup({
    AuthorizedOn,
    isBackup,
    backupWords,
}: {
    AuthorizedOn: (mnemonic: string) => void;
    isBackup?: boolean;
    backupWords?: string;
}) {
    const [stepOfProcess, setStepOfProcess] = useState(1);
    const [mnemonic, setMnemonic] = useState<string[]>([]);

    useEffect(() => {
        if (isBackup) {
            setMnemonic(backupWords!.split(' '));
        } else {
            let newMnemonic = generateMnemonicWords().split(' ');
            setMnemonic(newMnemonic);
        }
    }, []);

    const nextStep = (step: number) => {
        setStepOfProcess(step);
    }

    if (stepOfProcess === 2) {
        return <VerifyWalletPopup mnemonic={mnemonic} nextStep={() => nextStep(3)} prevStep={() => nextStep(1)} />
    }

    if (stepOfProcess === 3) {
        return <EncryptWalletPopup mnemonic={joinMnemonicValue(mnemonic)} AuthorizedOn={AuthorizedOn}/>
    }

    return (
        <div className={styles['create-wallet-popup']}>
            <PageTitle label={isBackup ? 'Backup Wallet' : 'Create Wallet'} />
            <div className={styles['create-wallet-popup-content']}>
                <p>
                    <span>
                        These are your 12 seed words. Write down at least one
                        copy of them on paper. This will allow you to restore
                        the wallet from the blockchain.
                    </span>
                    <span>
                        Make sure to spell every word correctly and copy them in
                        the correct order.
                    </span>
                </p>
                <div className={styles['mnemonic']}>
                    {mnemonic.map((el, index) => (
                        <div key={index} className={styles['mnemonic-item']}>
                            <>
                                <span className={styles['label']}>
                                    {++index}.
                                </span>
                                <span className={styles['mnemonic-key']}>
                                    {el}
                                </span>
                            </>
                        </div>
                    ))}
                    { isBackup || 
                        <SimpleButton
                            label={'Create'}
                            type={'colored'}
                            color={'primary'}
                            isSubmit={true}
                            onClick={() => nextStep(2)}
                        />
                    }
                </div>
            </div>
        </div>
    );
}
