import { ChangeEvent, FormEvent, ReactElement, useState } from 'react';
import InputField from '../../atoms/input-field';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';
import { MAX_INT64, getRawAmount, isNullOrEmpty, parseAmountWithDecimals } from '@renderer/utils/utils';
import { useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import { buildAndSignMeltTransaction } from '@renderer/core/wallet/txUtils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import SendConfirmationPopup from './send-confirmation-popup';
import bigDecimal from 'js-big-decimal';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';

export default function MeltPopup({
    token,
    ticker,
    balance,
    currentSupply,
    decimals,
    close,
}: {
    token: string;
    ticker: string;
    balance: bigint;
    currentSupply: string;
    decimals: number;
    close: () => void;
}) {
    const [meltAmount, setMeltAmount] = useState('');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');
    const [loading, setLoading] = useState(false);

    const [showPopUp, setShowPopUp] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [tx, setTx] = useState<Transaction>();

    const amountRegx1 = decimals ? new RegExp(`^0\.[0-9]{1,${decimals}}$`) : /^[1-9][0-9]*$/;
    const amountRegx2 = decimals ? new RegExp(`^[1-9][0-9]*(\.[0-9]{1,${decimals}})?$`) : /^[1-9][0-9]*$/;

    const wallet = useAppSelector(walletState);

    const handleChangeSupply = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.value === '0' || amountRegx1.test(e.target.value) || amountRegx2.test(e.target.value)) {
            setMeltAmount(e.target.value)
        } else if (isNullOrEmpty(e.target.value)) {
            setMeltAmount('');
        }
    };

    const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        try {
            if (!amountRegx1.test(meltAmount) && !amountRegx2.test(meltAmount)) {
                throw new Error("Invalid Amount.");
            }
            

            let rawAmount = getRawAmount(meltAmount, decimals);
            if (BigInt(rawAmount) > balance) {
                throw new Error(`Cannot Melt more than your balance`);
            }

            if (BigInt(rawAmount) > MAX_INT64) {
                throw new Error(`Cannot Melt more than ${parseAmountWithDecimals(MAX_INT64, decimals)} in single transaction`);
            }

            setLoading(true);
            let tx = await buildAndSignMeltTransaction(wallet.keys, rawAmount, token);

            setTx(tx);
            setShowPopUp(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
            setShowAlert(true);
        } finally {
            setLoading(false);
        }
    };

    const confirmTx = (idem: string) => {
        setShowPopUp(false);
        setAlertMessage(<><div>Transaction sent succesfully.</div><br/><div>IDEM:</div><div style={{wordBreak: 'break-all'}}>{idem}</div></>);
        setShowSuccess(true);
    }

    const closeSuccess = () => {
        close();
    }

    return (
        <div className={styles['change-supply-popup']}>
            <PageTitle label={'Change Supply'} />
            <div className={styles['change-supply-popup-content']}>
                <p>
                    Select the amount of tokens you would like to melt.
                </p>
                <form onSubmit={handleSubmit}>
                    {currentSupply && (
                        <>
                        <div className={styles['current-supply']}>
                            <h6 className={styles['label']}>Current Supply:</h6>
                            <p>{parseAmountWithDecimals(currentSupply, decimals)}</p>
                        </div>
                        <div className={styles['current-supply']}>
                            <h6 className={styles['label']}>Your Balance:</h6>
                            <p>{parseAmountWithDecimals(balance, decimals)}</p>
                        </div>
                        </>
                    )}
                    <InputField
                        label={'Melt Amount'}
                        size={'long'}
                        value={meltAmount}
                        required
                        onChange={handleChangeSupply}
                        inputType={'number'}
                        step="any"
                    />
                    <div className={styles['control-box']}>
                        <SimpleButton
                            label={'Melt'}
                            type={'colored'}
                            color={'kill'}
                            isSubmit={true}
                            isLoading={loading}
                        />
                    </div>
                </form>
            </div>
            { showPopUp &&
                <Popup close={() => setShowPopUp(false)} title={'Melting Confirmation'}>
                    <SendConfirmationPopup
                        token={token}
                        ticker={ticker}
                        amount={new bigDecimal(meltAmount).getPrettyValue()}
                        rawAmount={getRawAmount(meltAmount, decimals)}
                        tx={tx}
                        txType={TxTokenType.MELT}
                        confirm={confirmTx}
                    />
                </Popup>
            }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup isMini close={closeSuccess}>
                    <AlertPopup label='Confirmed' message={alertMessage} close={closeSuccess}/>
                </Popup>
            }
        </div>
    );
}
