import { AuthorityData } from '@renderer/components/atoms/token-card';
import PageTitle from '../../atoms/page-title';
import SimpleButton from '../../atoms/simple-button';
import Table from '../../organism/table';
import styles from './popups.module.css';
import { ReactElement, useState } from 'react';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import { useAppSelector } from '@renderer/store/hooks';
import { walletState } from '@renderer/store/slices/wallet';
import { buildAndSignKillTransaction } from '@renderer/core/wallet/txUtils';
import Popup from '@renderer/components/organism/popup';
import SendConfirmationPopup from './send-confirmation-popup';
import AlertPopup from './alert-popup';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';
import { PermissionLable } from '@renderer/components/atoms/permission-toggle';

const tableKillAuthorityHeaders = [
    'Authorise',
    'Mint',
    'Melt',
    'Rescript',
    'Subgroup',
];

export default function KillAuthorityPopup({
    token,
    data,
    allAuths,
    close
}: {
    token: string;
    data: AuthorityData;
    allAuths: AuthorityData[];
    close: () => void;
}) {

    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState<ReactElement | string>('');
    const [loading, setLoading] = useState(false);

    const [showPopUp, setShowPopUp] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [tx, setTx] = useState<Transaction>();

    const wallet = useAppSelector(walletState);

    const getDataOfPermissionsKill = () => {
        let auths = allAuths.filter(a => a.outpoint !== data.outpoint);

        let remaining = {
            authorise: auths.some(a => a.authorise),
            melt: auths.some(a => a.melt),
            mint: auths.some(a => a.mint),
            rescript: auths.some(a => a.rescript),
            subgroup: auths.some(a => a.subgroup),
        };

        const result = [data, remaining];
        return result;
    };

    const tableData = getDataOfPermissionsKill();

    const handleKill = async () => {
        try {            
            setLoading(true);
            let tx = await buildAndSignKillTransaction(wallet.keys, data.outpoint);

            setTx(tx);
            setShowPopUp(true);
        } catch (e) {
            setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
            setShowAlert(true);
        } finally {
            setLoading(false);
        }
    }
    
    const confirmTx = (idem: string) => {
        setShowPopUp(false);
        setAlertMessage(<><div>Transaction sent succesfully.</div><br/><div>IDEM:</div><div style={{wordBreak: 'break-all'}}>{idem}</div></>);
        setShowSuccess(true);
    }

    const closeSuccess = () => {
        close();
    }

    const permsStr = () => {
        let parts: PermissionLable[] = [];
        if (data.authorise) {
            parts.push('authorise')
        }
        if (data.mint) {
            parts.push('mint')
        }
        if (data.melt) {
            parts.push('melt')
        }
        if (data.rescript) {
            parts.push('rescript')
        }
        if (data.subgroup) {
            parts.push('subgroup')
        }
        return parts.join(', ')
    }

    return (
        <div className={styles['kill-authority-popup']}>
            <PageTitle label={'Kill Authority'} />
            <div className={styles['kill-authority-popup-content']}>
                <p>
                    You are about to kill an authority UTXO. This will remove
                    this authority from the UTXO set. See below for the
                    permissions that the selected UTXO has and what permissions
                    will remain after it has been destroyed.
                </p>
                <div className={styles['table-container']}>
                    <Table
                        headers={tableKillAuthorityHeaders}
                        data={tableData}
                        tableType={'kill_authority'}
                    />
                </div>
                <div className={styles['control-box']}>
                    <SimpleButton
                        label={'Kill Authority'}
                        type={'colored'}
                        color={'kill'}
                        onClick={handleKill}
                        isLoading={loading}
                    />
                </div>
            </div>
            { showPopUp &&
                <Popup close={() => setShowPopUp(false)} title={'Kill Authority Confirmation'}>
                    <SendConfirmationPopup
                        token={token}
                        ticker={''}
                        amount={"0"}
                        rawAmount={"0"}
                        auths={permsStr()}
                        tx={tx}
                        txType={TxTokenType.MELT}
                        confirm={confirmTx}
                    />
                </Popup>
            }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
            { showSuccess &&
                <Popup isMini close={closeSuccess}>
                    <AlertPopup label='Confirmed' message={alertMessage} close={closeSuccess}/>
                </Popup>
            }
        </div>
    );
}
