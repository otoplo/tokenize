import InputField from '@renderer/components/atoms/input-field';
import SimpleButton from '../../atoms/simple-button';
import styles from './popups.module.css';
import { ChangeEvent, useState } from 'react';
import { isPasswordValid } from '@renderer/core/wallet/seedUtils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';
import Skeleton from 'react-loading-skeleton';
import { broadcastTransaction, saveLocalTransaction } from '@renderer/core/wallet/txUtils';
import Transaction from 'nexcore-lib/types/lib/transaction/transaction';
import { parseAmountWithDecimals } from '@renderer/utils/utils';
import { TxTokenType } from '@renderer/core/wallet/walletUtils';
import { TxEntityState } from '@renderer/core/db/entities';
import NiftyProvider from '@renderer/providers/niftyProvider';

interface InterfaceSendConfirmationPopup {
    token?: string;
    ticker: string;
    destination?: string;
    amount: string;
    rawAmount: string;
    auths?: string;
    tx?: Transaction;
    txType: TxTokenType;
    confirm: (idem: string) => void;
}

export default function SendConfirmationPopup({
    token,
    ticker,
    destination,
    amount,
    rawAmount,
    auths,
    tx,
    txType,
    confirm,
}: InterfaceSendConfirmationPopup) {

    const [password, setPassword] = useState('');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');
    const [loading, setLoading] = useState(false);

    const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };

    const validateAndBroadcast = async () => {
        if (password && tx) {
            if (isPasswordValid(password)){
                try {
                    setLoading(true);
                    let idem = await broadcastTransaction(tx.serialize());
                    let dest = destination || 'Payment to yourself';
                    let state: TxEntityState = destination ? 'outgoing' : 'both';
                    let parent = '';
                    if (token && NiftyProvider.isNiftySubgroup(token)) {
                        parent = NiftyProvider.NIFTY_TOKEN.token;
                    }
                    await saveLocalTransaction(tx, idem, dest, rawAmount, state, token ? txType : TxTokenType.NO_GROUP, token, parent);
                    confirm(idem);
                } catch (e) {
                    setAlertMessage(e instanceof Error ? e.message : "An error occured, try again later.");
                    setShowAlert(true);
                } finally {
                    setLoading(false);
                }
            } else {
                setAlertMessage("Wrond password.");
                setShowAlert(true);
            }
        }
    }

    return (
        <div className={styles['send-confirmation-popup']}>
            <p>
                Check that these details are correct before confirming the transaction.
            </p>
            <div className={styles['content-details']}>
                {token && 
                    <p>
                        <span>Token:</span>
                        <span className={styles['content-break']}>{token}</span>
                    </p>
                }
                {destination && 
                    <p>
                        <span>Destination:</span>
                        {destination}
                    </p>
                }
                { !auths ? (
                    <p>
                        <span>Amount:</span>
                        {amount} {ticker}
                    </p>
                ) : (
                    <p>
                        <span>Authorities:</span>
                        {auths}
                    </p>
                )}
                
                <p>
                    <span>Fee:</span>
                    {parseAmountWithDecimals(tx?.getFee() ?? -1, 2)} NEXA
                </p>
            </div>
            <div>
                <InputField
                    label={'Password'}
                    size={'long'}
                    onChange={handlePasswordChange}
                    inputType={'password'}
                />
            </div>
            { loading ? (
                <span className={styles['skeleton']}>
                    <Skeleton width={250} height={50} borderRadius={25}/>
                </span>
            ) : (
                <SimpleButton
                    label={'Confirm'}
                    type={'colored'}
                    color={'primary'}
                    onClick={validateAndBroadcast}
                />
            ) }
            { showAlert &&
                <Popup isTiny close={() => setShowAlert(false)}>
                    <AlertPopup label='Error' message={alertMessage} close={() => setShowAlert(false)}/>
                </Popup>
            }
        </div>
    );
}
