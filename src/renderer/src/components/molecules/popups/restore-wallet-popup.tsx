import { ChangeEvent, FormEvent, useState } from 'react';
import PageTitle from '../../atoms/page-title';
import InputField from '../../atoms/input-field';
import SimpleButton from '../../atoms/simple-button';
import EncryptWalletPopup from './encrypt-wallet-popup';
import styles from './popups.module.css';
import { isMnemonicValid, joinMnemonicValue } from '../../../core/wallet/seedUtils';
import { isNullOrEmpty } from '../../../utils/utils';
import Popup from '@renderer/components/organism/popup';
import AlertPopup from './alert-popup';

const emptyMnemonic = {
    1: '',
    2: '',
    3: '',
    4: '',
    5: '',
    6: '',
    7: '',
    8: '',
    9: '',
    10: '',
    11: '',
    12: '',
};

export default function RestoreWalletPopup({
    AuthorizedOn,
}: {
    AuthorizedOn: (mnemonic: string) => void;
}) {
    const [stepOfProcess, setStepOfProcess] = useState<number>(1);
    const [mnemonic, setMnemonic] = useState(emptyMnemonic);
    const [mnemonicValue, setMnemonicValue] = useState("");

    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');

    const arrayFromObject = Object.entries(mnemonic).map(([key, value]) => ({
        [key]: value,
    }));

    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        let vals = Object.values(mnemonic);
        for (let v of vals) {
            if (isNullOrEmpty(v) || v.includes(' ')) {
                setAlertMessage("Invalid Seed.");
                setShowAlert(true);
                return;
            }
        }

        let words = joinMnemonicValue(vals);
        if (!isMnemonicValid(words)) {
            setAlertMessage("Invalid Seed.");
            setShowAlert(true);
            return;
        }
        
        setMnemonicValue(words);
        setStepOfProcess(2);
    };

    const handleInputChange = (key: string, value: string) => {
        setMnemonic((prevState) => ({
            ...prevState,
            [key]: value.trim().toLowerCase(),
        }));
    };

    return (
        <>
            {stepOfProcess === 1 ? (
                <div className={styles['restore-wallet-popup']}>
                    <PageTitle label={'Restore Wallet'} />
                    <div className={styles['restore-wallet-popup-content']}>
                        <p>
                            <span>
                                Enter your 12 seed words to restore a wallet
                                from the blockchain.
                            </span>
                            <span>
                                Make sure to spell every word correctly and
                                enter them in the correct order.
                            </span>
                        </p>
                        <form onSubmit={handleSubmit}>
                            {arrayFromObject?.map((el) => (
                                <InputField
                                    key={Object.keys(el)[0]}
                                    label={Object.keys(el)[0]}
                                    size={'short'}
                                    dot={true}
                                    onChange={(
                                        e: ChangeEvent<HTMLInputElement>,
                                    ) =>
                                        handleInputChange(
                                            Object.keys(el)[0],
                                            e.target.value,
                                        )
                                    }
                                />
                            ))}
                            <SimpleButton
                                label={'Restore'}
                                type={'colored'}
                                color={'primary'}
                                isSubmit={true}
                            />
                        </form>
                    </div>
                    { showAlert &&
                        <Popup isTiny close={() => setShowAlert(false)}>
                            <AlertPopup label='Failed' message={alertMessage} close={() => setShowAlert(false)}/>
                        </Popup>
                    }
                </div>
            ) : (
                <EncryptWalletPopup mnemonic={mnemonicValue} AuthorizedOn={AuthorizedOn}/>
            )}
        </>
    );
}
