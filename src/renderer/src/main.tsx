import ReactDOM from 'react-dom/client'
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { SkeletonTheme } from 'react-loading-skeleton';
import { store } from './store/store';
import App from './App';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './index.css';
import './reset.css';
import 'react-loading-skeleton/dist/skeleton.css';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
      <MemoryRouter>
          <SkeletonTheme
              baseColor="rgba(142, 143, 143, 0.4)"
              highlightColor="rgba(255, 255, 255, 0.1)"
              duration={2}
          >
              <App />
          </SkeletonTheme>
          <ToastContainer limit={3} newestOnTop={true} pauseOnFocusLoss={false}/>
      </MemoryRouter>
  </Provider>
)
