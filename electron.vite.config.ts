import { resolve } from 'path';
import { defineConfig } from 'electron-vite';
import react from '@vitejs/plugin-react';
import svgr from 'vite-plugin-svgr';
import { nodePolyfills } from 'vite-plugin-node-polyfills';

export default defineConfig({
  main: {
    plugins: []
  },
  preload: {
    plugins: []
  },
  renderer: {
    resolve: {
      alias: {
        '@renderer': resolve('src/renderer/src'),
      }
    },
    plugins: [
      react(),
      svgr(),
      nodePolyfills({
        protocolImports: true,
        globals: {
          global: true,
          process: true,
          Buffer: true
        }
      })
    ],
    server: { // dev server
      port: 3006,
      proxy: {
        "/_public": {
          target: 'https://niftyart.cash',
          changeOrigin: true,
        } 
      }
    }
  }
})
