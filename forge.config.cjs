const { version } = require('./package.json');
const archiver = require('archiver');
const fs = require('fs');

async function createArchive(dirPath, archive, output) {
  // listen for all archive data to be written
  // 'close' event is fired only when a file descriptor is involved
  output.on('close', function() {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  // This event is fired when the data source is drained no matter what was the data source.
  // It is not part of this library but rather from the NodeJS Stream API.
  // @see: https://nodejs.org/api/stream.html#stream_event_end
  output.on('end', function() {
    console.log('Data has been drained');
  });

  // good practice to catch warnings (ie stat failures and other non-blocking errors)
  archive.on('warning', function(err) {
    if (err.code === 'ENOENT') {
      // log warning
      console.warn('got ENOENT');
    } else {
      // throw error
      throw err;
    }
  });

  // good practice to catch this error explicitly
  archive.on('error', function(err) {
    throw err;
  });

  // pipe archive data to the file
  archive.pipe(output);

  // append files from a sub-directory and naming it `new-subdir` within the archive
  archive.directory(`${dirPath}/`, dirPath.substring(dirPath.lastIndexOf('tokenize-')));

  // finalize the archive (ie we are done appending files but streams have to finish yet)
  // 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
  await archive.finalize();
}

async function createTarGz(dirPath) {
  const output = fs.createWriteStream(`${dirPath}.tar.gz`);
  const archive = archiver('tar', {
    gzip: true,
    gzipOptions: { level: 9 }
  });

  await createArchive(dirPath, archive, output);
}

async function createZip(dirPath) {
  // create a file to stream archive data to.
  const output = fs.createWriteStream(`${dirPath}.zip`);
  const archive = archiver('zip', {
    zlib: { level: 9 }
  });

  await createArchive(dirPath, archive, output);
}

module.exports = {
  packagerConfig: {
    name: "Tokenize",
    icon: './resources/icons/icon',
    appBundleId: 'com.otoplo.tokenize',
    ignore: [
      "^\\/src$",
      "^\\/node_modules$",
      "^\\/resources$",
      "^\\/[.].+",
      "^\\/.+\.md$",
      "^\\/tsconfig.*\.json$",
      "^\\/forge\.config\.cjs$",
      "^\\/electron\.vite\.config\.ts$"
    ],
    osxSign: {
      platform: "darwin",
    },
    osxNotarize: {
      tool: "notarytool",
      keychain: "~/Library/Keychains/login.keychain-db",
      keychainProfile: "otoplo-electron"
    }
  },
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-dmg',
      config: {
        icon: './resources/icons/icon.icns',
        name: `tokenize-osx-${version}`,
        overwrite: true
      }
    }
  ],
  hooks: {
    postPackage: async (forgeConfig, options) => {
      if (options.platform === 'darwin') {
        return;
      }

      const oldDirName = options.outputPaths[0];
      const newDirName = oldDirName.replace("Tokenize", "tokenize") + `-${version}`;

      try {
        fs.renameSync(oldDirName, newDirName);

        console.log('Directory renamed successfully.')

        if (options.platform.includes('win')) {
          await createZip(newDirName);
        } else {
          await createTarGz(newDirName);
        }
        
      } catch (err) {
        console.log(err)
      }
    }
  }
};
